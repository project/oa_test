Feature: Show Spaces Pane

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @pane @pane_spacegroups
  Scenario: Interact with sitemap
    When I visit the homepage in space "Pane Group Space @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new subspace"
      And I fill in "Title" with "PaneGroup Space Subspace @timestamp"
      And I fill in the following:
        | Editor              | plain_text          |
        | body[und][0][value] | Space body          |
      Then I press "edit-submit"
      Then I visit the homepage in space "Pane Group Space @timestamp"
      And I click "Customize this page"
      And I click "Add new pane"
      And I click "Open Atrium" in the "CTools modal"
      And I wait 2 seconds
      And I click to add pane "Spaces & Groups"
      And I press "Save" in the "CTools modal"
      And I wait for the Panels IPE to deactivate
      And I press "Save as custom"
      And I wait for AJAX to finish
      And I reload the page
      Then I should see the text "PaneGroup Space Subspace @timestamp" under ".view-open-atrium-groups"
