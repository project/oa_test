Feature: Have a Calendar with multiple space content
  In order to have a calendar of content form multiple spaces
  As a site administrator
  I am able to create events in visible in other groups

  Background:
    Given I am logged in as a user with the "administrator" role
    And users:
    | name                  | pass      | mail                              | roles    |
    | OtherSpaceVisibility  | ChangeMe! | OtherSpaceVisibility@example.com  |  2       |

  @api @javascript @oa_event @oa_event_other
  Scenario: Create event in subspace
    When I visit the homepage in space "Space Visibility @timestamp"
    And I visit the homepage in space "Secret Space @timestamp"
      And I click "Edit" in the "Contextual Tabs"
      And I select the radio button "Private - accessible only to space members"
      And I press "edit-submit"
    Then I should see the text "Private"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Calendar Section" in the "Main Region"
      And I fill in "Title" with "Secret Section @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Section body  |
      Then I press "edit-submit"
    Then I should see the text "the current month and year"
      Then I click "Create Content"
      Then I click "Create Event"
      And I fill in "Title" with "Secret Event @timestamp"
      And I fill in the following:
        | Editor                              | plain_text  |
        | body[und][0][value]                 | event body  |
        | field_oa_date[und][0][value][time]  | 10:30pm      |
        | field_oa_date[und][0][value2][time] | 11:30pm      |
      And I fill in "field_oa_date[und][0][value][time]" with "10:30pm"
      And I fill in "field_oa_date[und][0][value2][time]" with "11:30pm"
      And I fill in "field_oa_date[und][0][value][date]" with "a date of +2 days"
      And I fill in "field_oa_date[und][0][value2][date]" with "a date of +2 days"
      And I click "Access" in the "Main Region"
      And I fill the select2 input for "form-item-oa-other-spaces-ref-und-0-default" with "Space Visibility @timestamp"
      And I select "Space Visibility @timestamp" from the dropdown results
      Then I press "edit-submit"
    And I visit the homepage in space "Other Space"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Calendar Section" in the "Main Region"
      And I fill in "Title" with "Section2 @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Section body  |
      Then I press "edit-submit"
    Then I should see the text "the current month and year"
      Then I click "Create Content"
      Then I click "Create Event"
      And I fill in "Title" with "Public Event @timestamp"
      And I fill in the following:
        | Editor                              | plain_text  |
        | body[und][0][value]                 | event body  |
        | field_oa_date[und][0][value][time]  | 2:30pm      |
        | field_oa_date[und][0][value2][time] | 3:30pm      |
      And I fill in "field_oa_date[und][0][value][time]" with "2:30pm"
      And I fill in "field_oa_date[und][0][value2][time]" with "3:30pm"
      And I click "Access"
      And I fill the select2 input for "form-item-oa-other-spaces-ref-und-0-default" with "Space Visibility @timestamp"
      And I select "Space Visibility @timestamp" from the dropdown results
      Then I press "edit-submit"
      And I click "subspace-dropdown"
      And I click "Section2 @timestamp"
      Then I click "Create Content"
      Then I click "Create Event"
      And I fill in "Title" with "Public Event2 @timestamp"
      And I fill in the following:
        | Editor                              | plain_text  |
        | body[und][0][value]                 | event body  |
        | field_oa_date[und][0][value][time]  | 8:30pm      |
        | field_oa_date[und][0][value2][time] | 9:30pm      |
      And I fill in "field_oa_date[und][0][value][time]" with "8:30pm"
      And I fill in "field_oa_date[und][0][value2][time]" with "9:30pm"
      And I fill in "field_oa_date[und][0][value][date]" with "a date of yesterday"
      And I fill in "field_oa_date[und][0][value2][date]" with "a date of yesterday"
      Then I press "edit-submit"
      And I click "subspace-dropdown"
      And I click "Section2 @timestamp"
    Then I should see the text "8:30pPublic Event2 @timestamp"
    Then I should see the text "2:30pPublic Event @timestamp"
      And I click "parent-dropdown"
      And I click "Space Visibility @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Calendar Section" in the "Main Region"
      And I fill in the following:
        | Title               | Calendar Section title |
        | Editor              | plain_text    |
        | body[und][0][value] | Section body  |
      Then I press "edit-submit"
    Then I should see the text "the current month and year"
      Then I click "Create Content"
      Then I click "Create Event"
      And I fill in "Title" with "Public Event3 @timestamp"
      And I fill in the following:
        | Editor                              | plain_text  |
        | body[und][0][value]                 | event body  |
        | field_oa_date[und][0][value][time]  | 6:30pm      |
        | field_oa_date[und][0][value2][time] | 7:30pm      |
      And I fill in "field_oa_date[und][0][value][time]" with "6:30pm"
      And I fill in "field_oa_date[und][0][value2][time]" with "7:30pm"
      And I fill in "field_oa_date[und][0][value][date]" with "a date of tomorrow"
      And I fill in "field_oa_date[und][0][value2][date]" with "a date of tomorrow"
      Then I press "edit-submit"
      And I click "parent-dropdown"
      And I click "Space Visibility @timestamp"
        And I click "Customize this page"
      And I click "Add new pane"
      And I click "Open Atrium Events" in the "CTools modal"
      And I click to add pane "View: Open Atrium Calendar: Calendar"
      And I check the box "exposed[og_subspaces_view_all]"
      And I press "Save" in the "CTools modal"
      And I wait for the Panels IPE to deactivate
      And I press "Save as custom"
      And I wait for AJAX to finish
      And I reload the page
    Then I should see the text "6:30pPublic Event3 @timestamp"
    Then I should see the text "2:30pPublic Event @timestamp"
    Then I should see the text "10:30pSecret Event @timestamp"
    Then I should not see the text "8:30pPublic Event2 @timestamp"
      Then I log in with "OtherSpaceVisibility"
      And I visit the homepage in space "Space Visibility @timestamp"
    Then I should see the text "6:30pPublic Event3 @timestamp"
    Then I should see the text "2:30pPublic Event @timestamp"
    Then I should not see the text "8:30pPublic Event2 @timestamp"
    Then I should not see the text "10:30pSecret Event"