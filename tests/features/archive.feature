Feature: Archive content

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @oa_archive
  Scenario: Archive discussion
    When I visit the homepage in space "Discussion Space @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Discussion Section" in the "Main Region"
      And I fill in "Title" with "Discussion Section Title @timestamp"
      Then I press "Publish"
      Then I click "Create Content"
      Then I click "Create Discussion Post"
      And I fill in "Title" with "Discussion @timestamp"
      Then I press "Publish"
      Then I click "Create Content"
      Then I click "Create Discussion Post"
      And I fill in "Title" with "Discussion 2 @timestamp"
      Then I press "Publish"
      Then I visit the current section homepage
    Then I should see the text "Discussion @timestamp"
    Then I should see the text "Discussion 2 @timestamp"
      Then I click "Discussion @timestamp"
      And I click "Configure" in the "Contextual Tabs" region
      And I click "Archive This"
      Then I press "Archive This"
      Then I should see the text "Item has been archived"
      Then I visit the current section homepage
      Then I should not see the text "Discussion @timestamp"
      Then I should see the text "Discussion 2 @timestamp"
      And I click "Configure" in the "Contextual Tabs" region
      And I click "Content" in the "Contextual Tabs" region
    Then I should not see the text "Discussion @timestamp"
    Then I should see the text "Discussion 2 @timestamp"
      And I select "Yes" from "flagged"
      Then I press "Apply"
    Then I should see the text "Discussion @timestamp"
    Then I should see the text "Discussion 2 @timestamp"
      Then I click "Discussion @timestamp"
      And I click "Configure" in the "Contextual Tabs" region
      And I click "Remove from archive"
      Then I press "Remove from archive"
    Then I should see the text "Item removed from archive"
      Then I visit the current section homepage
    Then I should see the text "Discussion @timestamp"
    Then I should see the text "Discussion 2 @timestamp"
