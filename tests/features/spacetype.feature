Feature: Space Types

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @oa_space @space_type @space_type_single
  Scenario: Add Space type and Customize permissions
    When I visit "admin/structure/taxonomy/space_type"
      Then I click "Add term"
      Then I fill in "name" with "Space Type @timestamp"
      Then I press "Save"
      Then I visit "node/add/oa-space"
      Then I should see the link "Space Type @timestamp" in the "Main Region"
      Then I click "Space Type @timestamp" in the "Main Region"
      Then I should see the heading "Create Space Type @timestamp"
      Then I fill in "Title" with "Space Type Test @timestamp"
      Then I click "Inheritance"
      Then I select "Override default roles and permissions" from "Group roles and permissions"
      Then I press "Publish"
      Then the link "Create new Space Type @timestamp" should exist
      Then the link "Create new Default" should exist
      And I click "Configure" in the "Contextual Tabs" region
      And I click "Config" in the "Configure Drop Down" region
      Then I click "Permissions" in the "Main Region"
      Then I uncheck permission "Use any space type" for "administrator member"
      Then I check permission "Use Default space type" for "member"
      Then I check permission "Create Space content" for "member"
      Then I press "Save permissions"
      Then I log in with a member of "Space Type Test @timestamp"
    When I visit the homepage in space "Space Type Test @timestamp"
      And I click "subspace-dropdown"
      Then the link "Create new Space Type @timestamp" should not exist
      Then the link "Create new Default" should exist
      Then I click "Create new subspace"
      Then I should see "Create Default Space"
      Then I click "parent-dropdown"
      Then I click "Site map"
      And I select "Space Type Test @timestamp" from sitemap dropdown
      And I click "New Subspace"
      And I click "Space Type2"
      Then the "Space Blueprint" select box should have option "Default"
      Then the "Space Blueprint" select box should not have option "Space Type @timestamp"
      When I visit "create space url for 'Space Type @timestamp'"
      Then I should see the heading "Access Denied"

  @api @javascript @oa_space @space_type @space_type_two
  Scenario: Add Space type and Customize permissions
    When I visit "admin/structure/taxonomy/space_type"
      Then I click "Add term"
      Then I fill in "name" with "Space Type @timestamp"
      Then I press "Save"
      Then I fill in "name" with "Space Type 2 @timestamp"
      Then I press "Save"
      Then I visit "node/add/oa-space"
      Then I should see the link "Space Type @timestamp" in the "Main Region"
      Then I should see the link "Space Type 2 @timestamp" in the "Main Region"
      Then I should see the link "Default" in the "Main Region"
      Then I click "Space Type @timestamp" in the "Main Region"
      Then the link "Create new Space Type @timestamp" should exist
      Then the link "Create new Space Type 2 @timestamp" should exist
      Then the link "Create new Default" should exist
      Then I should see the heading "Create Space Type @timestamp"
      Then I fill in "Title" with "Space Type Test @timestamp"
      Then I click "Inheritance"
      Then I select "Override default roles and permissions" from "Group roles and permissions"
      Then I press "Publish"
      And I click "Configure" in the "Contextual Tabs" region
      And I click "Config" in the "Configure Drop Down" region
      Then I click "Permissions" in the "Main Region"
      Then I uncheck permission "Use any space type" for "administrator member"
      Then I check permission "Use Default space type" for "member"
      Then I check permission "Use Space Type @timestamp space type" for "member"
      Then I check permission "Create Space content" for "member"
      Then I press "Save permissions"
      Then I log in with a member of "Space Type Test @timestamp"
    When I visit the homepage in space "Space Type Test @timestamp"
      And I click "subspace-dropdown"
      Then the link "Create new Space Type @timestamp" should exist
      Then the link "Create new Space Type 2 @timestamp" should not exist
      Then the link "Create new Default" should exist
      Then I click "Create new subspace"
      Then I should see the link "Space Type @timestamp" in the "Main Region"
      Then I should not see the link "Space Type 2 @timestamp" in the "Main Region"
      Then I should see the link "Default" in the "Main Region"
      Then I click "parent-dropdown"
      Then I click "Site map"
      And I select "Space Type Test @timestamp" from sitemap dropdown
      And I click "New Subspace"
      And I click "Space Type2"
      Then the "Space Blueprint" select box should have option "Default"
      Then the "Space Blueprint" select box should have option "Space Type @timestamp"
      Then the "Space Blueprint" select box should not have option "Space Type 2 @timestamp"
      When I visit "create space url for 'Space Type @timestamp'"
      Then I should see the heading "Create Space Type @timestamp"
      When I visit "create space url for 'Default'"
      Then I should see the heading "Create Default Space"
      When I visit "create space url for 'Space Type 2 @timestamp'"
      Then I should see the heading "Access Denied"
