Feature: Have notifications default correctly.

  Background:
    Given I am logged in as a user with the "administrator" role
  
  @api @javascript @oa_notifications @oa_notifications_defaults
  Scenario: Interact with multiple level
    When I go to "node/add/oa-group"
      And I fill in "Title" with "Parent Group 1 @timestamp"
      Then I press "edit-submit"
    Then I should see the heading "Parent Group 1 @timestamp"
    When I go to "node/add/oa-group"
      And I fill in "Title" with "Parent Group 2 @timestamp"
      Then I press "edit-submit"
    Then I should see the heading "Parent Group 2 @timestamp"
      And I visit "node/add/oa-space"
      And I fill in "Title" with "Parent Space 1 @timestamp"
      Then I fill the select2 input for "form-item-oa-notifications-notifications-group" with "Parent Group 1 @timestamp"
      And I select "Parent Group 1 @timestamp" from the dropdown results
      Then I press "edit-submit"

      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Calendar Section" in the "Main Region"
      And I fill in "Title" with "Calendar Section @timestamp"
      Then the checkbox "oa_notifications[override]" should not be checked
      Then the ".form-item-oa-notifications-notifications-group .select2-search-choice div" element should contain "Select2 value for group 'Parent Group 1 @timestamp'"
      Then I fill the select2 input for "form-item-field-oa-group-ref-und" with "Parent Group 1 @timestamp"
      And I select "Parent Group 1 @timestamp" from the dropdown results
      Then I press "edit-submit"
      Then I click on a the calendar for "today"
      Then the ".form-item-oa-notifications-notifications-group .select2-search-choice div" element should contain "Select2 value for group 'Parent Group 1 @timestamp'"
      Then I fill in "Title" with "Event title @timestamp"
      Then I press "Publish"
      Then I should see the text "Notifications sent to 1 user"
      Then I should see the text "Event title @timestamp"

      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Calendar Section" in the "Main Region"
      And I fill in "Title" with "Calendar Section 2 @timestamp"
      Then the checkbox "oa_notifications[override]" should not be checked
      Then the ".form-item-oa-notifications-notifications-group .select2-search-choice div" element should contain "Select2 value for group 'Parent Group 1 @timestamp'"
      Then I check the box "oa_notifications[override]"
      Then I wait for AJAX to finish
      Then I fill the select2 input for "form-item-oa-notifications-notifications-group" with "Parent Group 2 @timestamp"
      And I select "Parent Group 2 @timestamp" from the dropdown results
      Then I press "Publish"

      Then I click on a the calendar for "today"
      Then the ".form-item-oa-notifications-notifications-group .select2-search-choice div" element should contain "Select2 value for group 'Parent Group 2 @timestamp'"
      Then I fill in "Title" with "Event title 2 @timestamp"
      Then I press "Publish"
      Then I should see the text "Notifications sent to 1 user"
      Then I should see the text "Event title 2 @timestamp"

      And I click "subspace-dropdown"
      And I click "Create new subspace"
      Then the ".form-item-oa-notifications-notifications-group .select2-search-choice div" element should contain "Select2 value for group 'Parent Group 1 @timestamp'"
