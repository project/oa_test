Feature: Customize Space
  In order to have a sitemap
  As a site administrator
  I am able to interact with it

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @oa_space @sitemap
  Scenario: Interact with sitemap
    When I visit the homepage in space "Sitemap Space @timestamp"
      And I click "Customize this page"
      And I click "Add new pane"
      And I click "OA Admin" in the "CTools modal"
      And I wait 2 seconds
      And I click to add pane "Site Map (App)"
      And I press "Finish" in the "CTools modal"
      And I wait for the Panels IPE to deactivate
      And I press "Save as custom"
      And I wait for AJAX to finish
      And I reload the page
      And I click "New Section"
      And I fill in "Title" with "Document Section @timestamp"
      And I click "Section Type2"
      And I select "Default" from "Section Type"
      And I click "Section Visibility3"
      And I press "Publish" in the "Tab3"
    Then I should see the heading "Document Section @timestamp"
    And I select top level from sitemap dropdown
      And I should see the link "New Space"
      And I click "New Space"
      And I fill in "Title" with "New Space @timestamp"
      And I click "Space Access3"
      And I press "Publish" in the "Tab3"
      And I wait for AJAX to finish
    Then I should see the heading "New Space @timestamp"
      And I select "New Space @timestamp" from sitemap dropdown
      And I click "New Subspace"
      And I fill in "Title" with "New Subspace @timestamp"
      And I click "Space Access3"
      And I press "Publish" in the "Tab3"
      And I wait for AJAX to finish
    Then I should see the heading "New Subspace @timestamp"
