Feature: Anonymous user login from home page
In order to access content for authenticated users
As an anonymous user
I want to be able to login

  Background:
    Given users:
    | name     | pass      | mail             | roles    |
    | TestUser | ChangeMe! | foo@example.com  | 4        |
 
  @standard_login @api @javascript
  Scenario: Editor is able to login
    Given I am on "/"
    When I fill in "TestUser" for "edit-name"
    And I fill in "ChangeMe!" for "edit-pass"
    And I press "Log in"
    Then I should see "TestUser" in the "User badge" region
