Feature: Have a Discussion
  In order to have a discussion
  As a site administrator
  I am able to create discussions

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @oa_discussion
  Scenario: Create discussion
    When I visit the homepage in space "Discussion Space @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Discussion Section" in the "Main Region"
      And I fill in "Title" with "Discussion Section Title @timestamp"
      And I fill in the following:
        | Editor                      | plain_text    |
        | body[und][0][value] | Section body  |
    Then I press "Publish"
    Then I click "Create Content"
    Then I click "Create Discussion Post"
      And I fill in "Title" with "Discussion No Reply @timestamp"
      And I fill in the following:
        | Editor              | plain_text          |
        | body[und][0][value] | Discussion body     |
    Then I press "Publish"
    Then I visit the current section homepage
    Then I should see the text "Discussion No Reply @timestamp"
    Then I should see the text "TODAY"
    Then I should see the text "0 replies"
    Then I click "Create Content"
    Then I click "Create Discussion Post"
      And I fill in "Title" with "Discussion title @timestamp"
      And I fill in the following:
        | Editor                      | plain_text    |
        | body[und][0][value] | Discussion body  |
    Then I press "Publish"
      And I fill in the following:
        | Editor                      | plain_text    |
        | comment_body[und][0][value] | Response body  |
    Then I press "Save"
      And I fill in the following:
        | Editor                      | plain_text    |
        | comment_body[und][0][value] | Response body 2 |
    Then I press "Save"
    Then I should see the text "Response body 2"
      And I click "unpublish" for comment "2"
    Then I should see the text "Response body"
    Then I should see the link "publish" for comment "2"
      And I click "publish" for comment "2"
    Then I should see the link "unpublish" for comment "2"
      And I click "edit" for comment "2"
      And I fill in "Response body full length 2" for "comment_body[und][0][value]" in the "Modal Frame"
    Then I press "Save" in the "Modal Frame"
    Then I should see the text "Response body full length 2"
    Then I should not see the text "Response body 2"
      Then I log in with a member of "Discussion Space @timestamp"
      Then I visit the homepage in space "Discussion Space @timestamp"
      And I click "Discussion title @timestamp"
      And I fill in the following:
        | Editor                      | plain_text      |
        | comment_body[und][0][value] | Response body 3 |
    Then I press "Save"
    Then I should see the text "Response body 3"
      Then I visit the homepage in space "Discussion Space @timestamp"
      And I click "Discussion Section Title @timestamp" in the "Main Region"
    Then I click "Create Content"
    Then I click "Create Discussion Post"
      And I fill in "Title" with "Discussion 2 title @timestamp"
      And I fill in the following:
        | Editor                      | plain_text         |
        | body[und][0][value] | Discussion body    |
    Then I press "Publish"
    Then I should see the text "Discussion 2 title @timestamp"
    Then I should see the text "Discussion body"
      And I fill in the following:
        | Editor                      | plain_text    |
        | comment_body[und][0][value] | Response body  |
    Then I press "Save"
      And I fill in the following:
        | Editor                      | plain_text    |
        | comment_body[und][0][value] | Response body 2 |
    Then I press "Save"
    Then I should see the text "Response body"
    Then I should see the text "Response body 2"
      And I click "unpublish" for comment "2"
    Then I should see the link "publish" for comment "2"
      Then I visit the current section homepage
    Then I should see the text "Discussion No Reply @timestamp"
    Then I should see the text "TODAY"
    Then I should see the text "0 replies"
    Then I should see the text "Discussion 2 title @timestamp"
    Then I should see the text "1 reply"
    Then I should see the text "Discussion title @timestamp"
    Then I should see the text "3 replies"

