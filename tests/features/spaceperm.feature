Feature: Space Permissions

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @oa_space @space_perm
  Scenario: Custom roles and permissions
    When I visit "node/add/oa-space"
      Then I fill in "Title" with "Space Perm @timestamp"
      Then I click "Inheritance"
      Then I select "Override default roles and permissions" from "Group roles and permissions"
      Then I press "Publish"
      And I click "Configure" in the "Contextual Tabs" region
      And I click "Config" in the "Configure Drop Down" region
      Then I click "Roles" in the "Main Region"
      Then I fill in "name" with "Test Role @timestamp"
      Then I press "Add role"
    Then I should see the text "The role has been added."
      Then I visit the homepage in space "Space Perm @timestamp"
      And I click "Configure" in the "Contextual Tabs" region
      And I click "Config" in the "Configure Drop Down" region
      Then I click "Permissions" in the "Main Region"
      Then I uncheck permission "Create Discussion Post content" for "member"
      Then I check permission "Create Discussion Post content" for "Test Role @timestamp"
      Then I press "Save permissions"
      Then I visit the homepage in space "Space Perm @timestamp"
      Then I have a member "Member @timestamp" of "Space Perm @timestamp"
      Then I log in with "Member @timestamp"
      Then I visit the homepage in space "Space Perm @timestamp"
      Then I visit "node/add/oa-discussion-post"
      Then I should see the heading "Access Denied"
      Then I log in with the owner of space "Space Perm @timestamp"
    When I visit the homepage in space "Space Perm @timestamp"
      And I click "Configure" in the "Contextual Tabs" region
      And I click "Config" in the "Configure Drop Down" region
      Then I click "People" in the "Main Region"
    Then I should see the text "current user name" in the "Main Region"
    Then I should see the text "Member @timestamp" in the "Main Region"
      Then I click the checkbox in the table row containing "Member @timestamp"
      Then I select "Modify OG user roles" from "operation"
      Then I press "Execute"
      Then I select "Test Role @timestamp" from "Add roles"
      Then I press "Next"
      Then I press "Confirm"
      Then I wait for AJAX to finish
      Then I should see the text "Test Role @timestamp" in the table row containing "Member @timestamp"
      Then I log in with "Member @timestamp"
      Then I visit the homepage in space "Space Perm @timestamp"
      Then I visit "node/add/oa-discussion-post"
      And I fill in "Title" with "Discussion title @timestamp"
      And I fill in the following:
        | Editor                      | plain_text    |
        | body[und][0][value] | Discussion body  |
    Then I press "Publish"