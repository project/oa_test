# test 1: Adding a section to the menu (and having it appear)
# Test: appear, auto populate title
Feature: Add content page
  In order to create a section with menu link
  As a site administrator
  I need to be able create a section page

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @oa_section
  Scenario: Add a section page excluded from menu
    When I visit "/node/add/oa-section/" in space
      Then I click "Calendar Section" in the "Main Region"
      And I fill in "Title" with "Section @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Section body  |
        And I uncheck the box "menu[hidden]"
    Then I press "edit-submit"
    Then the "h1" element should contain "Section @timestamp"
    Then I should see the link "Section @timestamp" in the "Section menu"
    Then I visit "/node/add/oa-section/"
      And I click "Calendar Section" in the "Main Region"
      And I fill in "Title" with "Section2 @timestamp"
      And I fill in the following:
        | Editor              | plain_text      |
        | body[und][0][value] | Section body    |
    Then I press "edit-submit"
    Then the "h1" element should contain "Section2 @timestamp"
    Then I should not see the link "Section2 @timestamp" in the "Section menu"

