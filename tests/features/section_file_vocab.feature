Feature: Have a File section based on vocabulary
  In order to have a media section
  As a site administrator
  I am able to create files

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @oa_files_vocab
  Scenario Outline: Adding Files to File Section
    When I visit the homepage in space "<space>"
      And I click "Configure" in the "Contextual Tabs" region
      And I click "Config" in the "Configure Drop Down" region
      And I click "Taxonomy" in the "Main Region"
      And I click "Add vocabulary" in the "Main Region"
      And I fill in "Name" with "Files Menu"
      And I check the box "og_vocab[node:oa_wiki_page][status]"
      And I press "Save"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Files Section" in the "Main Region"
      And I fill in "Title" with "File Section @timestamp"
      And I fill in the following:
        | Editor              | plain_text         |
        | body[und][0][value] | File Section body  |
      Then I press "edit-submit"
      And I click "Customize this page"
      And I click "Settings" for the pane that contains class "pane-oa-files-treeview"
      And I wait for AJAX to finish
      And I select "Vocabulary" from "oa_files_menu_mode"
      And I wait for AJAX to finish
      And I press "Finish" in the "CTools modal"
      And I wait for the Panels IPE to deactivate
      And I press "Save as custom"
      And I wait for AJAX to finish
      And I reload the page
      And I wait for AJAX to finish
      Then I click "New folder" in the "Files Header"
      And I fill in "foldername" with "Mew New Folder"
      And I press "Create Folder"
    Then I should see the text "Mew New Folder" in the "Main Region"
      And I click "New folder" for folder "Mew New Folder"
      And I fill in "foldername" with "Subfolder of Mew"
      And I press "Create Folder"
      Then I wait for AJAX to finish
    Then I should see the text "Subfolder of Mew" in the "Main Region"
    Then I should see the folder "Subfolder of Mew" under "Mew New Folder"
      And I click "Add file" for folder "Mew New Folder"
      And I switch to the frame "mediaBrowser"
      And I attach the file "<file>" via plupload
      And I go to next document creation step
      And I fill in "Title" with "First File"
      And I finish the document creation
    Then I should see the file "First file" under "Mew New Folder"
      And I click "Add file" for folder "Subfolder of Mew"
      And I switch to the frame "mediaBrowser"
      And I attach the file "<third_file>" via plupload
      And I go to next document creation step
      And I fill in "Title" with "Second file"
      And I finish the document creation
    Then I should see the file "First file" under "Mew New Folder"
    Then I should see the file "Second file" under "Subfolder of Mew"
      Then I click to Add file
      And I switch to the frame "mediaBrowser"
      And I attach the file "<second_file>" via plupload
      And I go to next document creation step
      And I fill in "Title" with "My File"
      And I finish the document creation
    Then I should see the text "My File"
      Then I click to Add file
      And I switch to the frame "mediaBrowser"
      And I attach the file "<second_file>" via plupload
      And I go to next document creation step
      And I check the box to create a new document
      And I wait for 2 seconds
      And I fill in "Title" with "My new File"
      And I finish the document creation
    Then I should see the text "My File"
    Then I should see the text "My new File"
      Then I visit the current space homepage
      Then I click "Create Content"
      Then I click link containing "Add File"
      And I switch to the frame "mediaBrowser"
      And I attach the file "<second_file>" via plupload
      And I go to next document creation step
      And I check the box to create a new document
      And I wait for 2 seconds
      And I fill in "Title" with "My next new File"
      And I finish the document creation
      And I click "subspace-dropdown"
      And I click "File Section @timestamp" in the "Breadcrumb"
    Then I should see the text "My File"
    Then I should see the text "My new File"
    Then I should see the text "My next new File"
      And I reload the page
    Then I should see the file "First file" under "Mew New Folder"
    Then I should see the text "My File"
    Then I should see the text "My new File"
    Then I should see the text "My next new File"
    Then I should see the folder "Subfolder of Mew" under "Mew New Folder"
    Then I should see the file "Second file" under "Subfolder of Mew"
    Then I should see the text "Mew New Folder" in the "Main Region"


Examples:
    | file           | second_file     | third_file | space       |
    | oa_splash.png  |  test-sm.png    | test.txt   | Random Text |
    | test-sm.png    | oa_splash.png   | test.txt   | Random Text |