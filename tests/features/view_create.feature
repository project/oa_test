Feature: I can create content pane views


  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @view_create
  Scenario: I create a view
    Given I am on "/admin/structure/views/add"
      And I fill in the following:
        | View name   | Behat Test    |
      And I uncheck the box "Create a page"
      And I press "Continue & edit"
      Then I click to add a views display
      Then I press "Content pane"
      Then I click add for views "Filter criteria" 
      Then I check the box "name[og_membership.og_group_ref_target_id]"
      Then I press "Apply (all displays)"
      Then I expose the form
      Then I press "Apply (all displays)"
      Then I click add for views "Filter criteria" 
      Then I check the box "name[field_data_oa_section_ref.oa_section_ref_target_id]"
      Then I press "Apply (all displays)"
      Then I expose the form
      Then I press "Apply (all displays)"
      Then I click "views-panel-pane-1-allow"
      Then I check the views checkbox "edit-allow-exposed-form"
      Then I select "Pane configuration form and exposed form" from "exposed_form_overrides[filters][og_group_ref_target_id]"
      Then I select "Pane configuration form and exposed form" from "exposed_form_overrides[filters][oa_section_ref_target_id]"
      Then I press "edit-submit"
      Then I press "Save"
    When I visit the homepage in space "View test @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Calendar Section" in the "Main Region"
      And I fill in "Title" with "Calendar Section @timestamp"
      Then I press "Publish"
      Then I click "Create Content"
      Then I click "Create Event"
      And I fill in "Title" with "Event @timestamp"
      And I fill in the following:
        | Editor                              | plain_text  |
        | body[und][0][value]                 | event body  |
      Then I press "Publish"
      Then I visit the current section homepage
      And I click "Customize this page"
      And I click "Add new pane"
      And I click "View panes" in the "CTools modal"
      And I click to add pane "View: Behat Test"
      And I select "Calendar Section @timestamp" from "oa_section_ref_target_id"
      And I press "Save" in the "CTools modal"
      And I wait for the Panels IPE to deactivate
      And I press "Save as custom"
      And I wait for AJAX to finish
      And I reload the page
      Then the "og_group_ref_target_id" field should contain "NID of space 'View test @timestamp'"
      Then the "oa_section_ref_target_id" field should contain "NID of 'Calendar Section @timestamp' in 'View test @timestamp'"
      And I click "Event @timestamp" on row "1" of view "behat_test"
      Then I should see the heading "Event @timestamp"
