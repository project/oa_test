Feature: Clone a space
  In order to have a cloned space
  As a site administrator
  I am able to create blueprint

  Background:
    Given I am logged in as a user with the "administrator" role
    And users:
    | name      | pass      | mail             | roles    |
    | TestUser  | ChangeMe! | foo@example.com  |  2       |

  @api @javascript @oa_space @oa_clone
  Scenario: Clone a space
    When I visit the homepage in space "Original Space @timestamp"
      Then I click "Members"
      And I fill in "name" with "TestUser"
      And I press the "Esc" key in the "User name" field
      Then I press "Add to space"
      And I click "Teams" in the "Main Region"
      And I click "Create New Team" in the "Main Region"
      Then I fill the select2 input for "field-name-field-oa-team-users" with "current user name"
      And I select "current user name" from the dropdown results
      Then I fill the select2 input for "field-name-field-oa-team-users" with "TestUser"
      And I select "TestUser" from the dropdown results
      And I fill in "Title" with "Original Team @timestamp"
      Then I press "edit-submit"
    Then I should see the text "current user name" in the "Main Region"
    Then I should see the text "TestUser" in the "Main Region"
      Then I press "Add to team"
    When I visit the homepage in space "Original Space @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Discussion Section" in the "Main Region"
      And I fill in "Title" with "Original Section Title @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Section body  |
      Then I press "edit-submit"
      Then I click "Create Content"
      Then I click "Create Discussion Post"
      And I fill in "Title" with "Original node title @timestamp|"
        And I fill in the following:
          | Editor              | plain_text    |
          | body[und][0][value] | Discussion body  |
      Then I press "edit-submit"
      Then I visit the homepage in space "Original Space @timestamp"
    Then I should see the text "Original Section Title @timestamp"
    Then I should see the text "Original node title @timestamp"
      And I click "Configure" in the "Contextual Tabs" region
      And I click "Clone this space"
      Then I press "edit-submit"
      And I wait for AJAX to finish
    Then I should see the text "Original Section Title @timestamp"
    Then I should see the text "Original node title @timestamp"
    Then I should see the text "Clone of Original Space"
      And I click "Configure" in the "Contextual Tabs" region
      And I click "Content" in the "Contextual Tabs" region
    Then I should see the text "Original Team @timestamp"
      Then I click "Original Team @timestamp"

  @api @javascript @oa_space @oa_blueprint
  Scenario: Clone a space
    When I visit the homepage in space "Original Space @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Discussion Section" in the "Main Region"
      And I fill in "Title" with "Original Section Title @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Section body  |
      Then I press "edit-submit"
      Then I click "Create Content"
      Then I click "Create Discussion Post"
      And I fill in "Title" with "Original node title @timestamp|"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Discussion body  |
      Then I press "edit-submit"
      Then I visit the homepage in space "Original Space @timestamp"
    Then I should see the text "Original Section Title @timestamp"
    Then I should see the text "Original node title @timestamp"
      And I click "Configure" in the "Contextual Tabs" region
      And I click "Create Blueprint from this Space"
      And I fill in the following:
        | Name               | Blueprint of Original Space |
      Then I press "edit-submit"
      And I click "parent-dropdown"
      And I click "Create new Space"
      And I click "Blueprint of Original Space" in the "Main Region"
      And I fill in "Title" with "Cloned space @timestamp"
      Then I press "edit-submit"
    Then I should see the text "Original Section Title @timestamp"
    Then I should see the text "Original node title @timestamp"
    Then I should see the text "Cloned space @timestamp"
