Feature: Simple files test

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @oa_files_simple
  Scenario: Adding Files to File Section
    When I visit the homepage in space "Random Text"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Files Section" in the "Main Region"
      And I fill in "Title" with "File Section @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | File Section body  |
      Then I press "edit-submit"
      Then I click to Add file
      And I wait for AJAX to finish
      And I switch to the frame "mediaBrowser"
      And I attach the file "oa_splash.png" via plupload
      And I go to next document creation step
      And I fill in "Title" with "OA Splash"
      And I finish the document creation
    Then I should see the text "OA Splash"
      Then I click to Add file
      And I switch to the frame "mediaBrowser"
      And I click "My files"
      And I select media item "oa_splash"
      And I switch to the frame "oaFilesUpload"
      And I check the box to create a new document
      And I wait for 2 seconds
      And I fill in the following:
        | Title               | Selected File |
      And I finish the document creation
    Then I should see the text "Selected File"
      Then I click "New folder" in the "Files Header"
      And I fill in "foldername" with "Mew New Folder"
      And I press "Create Folder"
    Then I should see the text "Mew New Folder" in the "Main Region"
      And I click "New folder" for folder "Mew New Folder"
      And I fill in "foldername" with "Subfolder of Mew"
      And I press "Create Folder"
    Then I should see the text "Subfolder of Mew" in the "Main Region"
    Then I should see the folder "Subfolder of Mew" under "Mew New Folder"
      And I click "Add file" for folder "Mew New Folder"
      And I switch to the frame "mediaBrowser"
        And I attach the file "test-sm.png" via plupload
      And I go to next document creation step
      And I fill in "Title" with "First file"
      And I finish the document creation
    Then I should see the file "First file" under "Mew New Folder"
      Then I press the file section cog
      And I wait for 1 second
      Then I click "Collapse all" in the "Main Region"
      Then I should not see the file "First file"
      Then I press the file section cog
      Then I click "Show filter" in the "Main Region"
      And I wait for 1 second
      Then I fill in "file_filter_text" with "First"
      Then I should see the file "First file"
      Then I should not see the file "OA Splash"
      Then I press "Clear"
      Then I should not see the file "First file"
      Then I should see the file "OA Splash"
