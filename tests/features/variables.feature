Feature: Override space variables

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @og_variables
  Scenario: Override variables
    When I visit the homepage in space "Variable test @timestamp"
      And I click "Configure" in the "Contextual Tabs" region
      And I click "Config" in the "Configure Drop Down" region
      And I click "Variables" in the "Main Region"
      And I click "Site information" in the "Main Region"
      And I fill in "Site name" with "Variable Test Site Name"
      Then I wait for AJAX to finish
      Then I should see the text "Autosaved: Site name saved as Variable Test Site Name"
      Then I visit the homepage in space "Variable test @timestamp"
      Then the title of the page should contain "Variable Test Site Name"
      And I click "subspace-dropdown"
      And I click "Create new subspace"
      And I fill in "Title" with "Variables test subspace @timestamp"
      Then I press "edit-submit"
      Then the title of the page should contain "Variable Test Site Name"
      And I click "Configure" in the "Contextual Tabs" region
      And I click "Config" in the "Configure Drop Down" region
      And I click "Variables" in the "Main Region"
      And I click "Site information" in the "Main Region"
      And I fill in "Site name" with "Variable Subspace Site Name"
      Then I wait for AJAX to finish
      Then I should see the text "Autosaved: Site name saved as Variable Subspace Site Name"
      Then I visit the homepage in space "Variables test subspace @timestamp"
      Then the title of the page should contain "Variable Subspace Site Name"
      Then I visit the homepage in space "Variable test @timestamp"
      Then the title of the page should contain "Variable Test Site Name"