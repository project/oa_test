Feature: Use My Spaces Checkbox

  Background:
    Given I am logged in as a user with the "administrator" role
    And users:
    | name      | pass      | mail             | roles    |
    | TestUser  | ChangeMe! | foo@example.com  |  2       |

  @api @javascript @oa_myspaces
  Scenario: Use my space checkbox
    When I visit the homepage in space "A Space @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Discussion Section" in the "Main Region"
      And I fill in "Title" with "Discussion Section 1 @timestamp"
      And I fill in the following:
        | Editor                      | plain_text    |
        | body[und][0][value] | Section body  |
    Then I press "Publish"
    Then I click "Create Content"
    Then I click "Create Discussion Post"
      And I fill in "Title" with "My Discussion @timestamp"
      And I fill in the following:
        | Editor              | plain_text          |
        | body[und][0][value] | Discussion body     |
    Then I press "Publish"
    When I visit the homepage in space "Another Space @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Discussion Section" in the "Main Region"
      And I fill in "Title" with "Discussion Section 2 @timestamp"
      And I fill in the following:
        | Editor                      | plain_text    |
        | body[und][0][value] | Section body  |
    Then I press "Publish"
    Then I click "Create Content"
    Then I click "Create Discussion Post"
      And I fill in "Title" with "My Discussion 2 @timestamp"
      And I fill in the following:
        | Editor              | plain_text          |
        | body[und][0][value] | Discussion body     |
    Then I press "Publish"
    When I visit the homepage in space "Another Space @timestamp"
      And I click "Customize this page"
      And I click "Add new pane"
      And I click "Open Atrium" in the "CTools modal"
      And I wait 2 seconds
      And I click to add pane "View: Open Atrium Comment Topic List: Topics"
      And I check the box "exposed[og_group_ref_target_id_mine]"
      And I press "Save" in the "CTools modal"
      And I wait for the Panels IPE to deactivate
      And I press "Save as custom"
      And I wait for AJAX to finish
    Then I should see the text "My Discussion 2 @timestamp" under ".view-oa-comment-topics.view-id-oa_comment_topics"
    Then I should see the text "My Discussion @timestamp" under ".view-oa-comment-topics.view-id-oa_comment_topics"
      Then I log in with a member of "Another Space @timestamp"
    When I visit the homepage in space "Another Space @timestamp"
    Then I should see the text "My Discussion 2 @timestamp" under ".view-oa-comment-topics.view-id-oa_comment_topics"
    Then I should not see the text "My Discussion @timestamp" under ".view-oa-comment-topics.view-id-oa_comment_topics"
      Then I log in with a member of "A Space @timestamp"
    When I visit the homepage in space "Another Space @timestamp"
    Then I should not see the text "My Discussion 2 @timestamp" under ".view-oa-comment-topics.view-id-oa_comment_topics"
    Then I should see the text "My Discussion @timestamp" under ".view-oa-comment-topics.view-id-oa_comment_topics"
    Then I log in with "TestUser"
    When I visit the homepage in space "Another Space @timestamp"
    Then I should not see the text "My Discussion 2 @timestamp" under ".view-oa-comment-topics.view-id-oa_comment_topics"
    Then I should not see the text "My Discussion @timestamp" under ".view-oa-comment-topics.view-id-oa_comment_topics"
