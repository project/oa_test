Feature: Have users access unpublished content
  In order to have a user able to see their unpublished content
  As a regular user
  I can see my content that is unpublished


  Background:
    Given I am logged in as a user with the "administrator" role
    And users:
    | name      | pass      | mail             | roles    |
    | TestUser  | ChangeMe! | foo@example.com  |  2       |

  @api @javascript @oa_access
  Scenario: User can see their unpublished content
    When I visit the homepage in private space "Normal Space @timestamp"
      Then I visit "/node/add/oa-discussion-post/"
      And I fill in "Title" with "Content Draft @timestamp"
      And I fill in the following:
        | Editor              | plain_text     |
        | body[und][0][value] | Body body      |
      And I fill in "Author" with "TestUser"
      And I press the "Esc" key in the "Author" field
      And I press "Save as draft"
      Then I visit "/node/add/oa-discussion-post/"
      And I fill in "Title" with "Content Published @timestamp"
      And I fill in the following:
        | Editor              | plain_text        |
        | body[und][0][value] | Body body         |
      And I fill in "Author" with "TestUser"
      And I press the "Esc" key in the "Author" field
      And I press "Publish"
      And I visit the homepage in space "Normal Space @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Discussion Section" in the "Main Region"
      And I fill in "Title" with "Discussion Section"
      And I fill in the following:
        | Editor              | plain_text         |
        | body[und][0][value] | Section body       |
      Then I press "edit-submit"
      Then I click "Create Content"
      Then I click "Create Discussion Post"
      And I fill in "Title" with "Content Draft Sectioned @timestamp"
      And I fill in the following:
        | Editor              | plain_text              |
        | body[und][0][value] | Body body               |
      And I fill in "Author" with "TestUser"
      And I press the "Esc" key in the "Author" field
      And I press "Save as draft"
      Then I click "Create Content"
      Then I click "Create Discussion Post"
      And I fill in "Title" with "Content Published Sectioned @timestamp"
      And I fill in the following:
        | Editor              | plain_text                  |
        | body[und][0][value] | Body body                   |
      And I fill in "Author" with "TestUser"
      And I press the "Esc" key in the "Author" field
      And I press "Publish"
      Then I log in with "TestUser"
      And I visit content "Content Draft @timestamp" in space "Normal Space @timestamp"
    Then I should see the text "Content Draft @timestamp"
    Then I should not see the text "Normal Space @timestamp"
      And I visit content "Content Published @timestamp" in space "Normal Space @timestamp"
    Then I should not see the text "Content Published @timestamp"
    Then I should not see the text "Normal Space @timestamp"
      And I visit content "Content Draft Sectioned @timestamp" in space "Normal Space @timestamp"
    Then I should see the text "Content Draft Sectioned @timestamp"
    Then I should not see the text "Normal Space @timestamp"
      And I visit content "Content Published Sectioned @timestamp" in space "Normal Space @timestamp"
    Then I should not see the text "Content Published Sectioned @timestamp"
    Then I should not see the text "Normal Space @timestamp"

