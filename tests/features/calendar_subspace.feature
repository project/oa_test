Feature: Have a Calendar in parent space
  In order to have a calendar of all subspace events
  As a site administrator
  I am able to create events in subspace

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @oa_event @oa_event_subspace
  Scenario: Create event in subspace
    When I visit the homepage in space "Calendar Space @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Calendar Section" in the "Main Region"
      And I fill in "Title" with "Calendar Section @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Section body  |
      Then I press "edit-submit"
    Then I should see the text "the current month and year"
      Then I click "Create Content"
      Then I click "Create Event"
      And I fill in "Title" with "Event @timestamp"
      And I fill in the following:
        | Editor                              | plain_text  |
        | body[und][0][value]                 | event body  |
        | field_oa_date[und][0][value][time]  | 2:30pm      |
        | field_oa_date[und][0][value2][time] | 3:30pm      |
      And I fill in "field_oa_date[und][0][value][time]" with "2:30pm"
      And I fill in "field_oa_date[und][0][value2][time]" with "3:30pm"
      Then I press "edit-submit"
    Then I should see the text "a full date of today"
 
      And I click "subspace-dropdown"
      And I click "Create new subspace"
      And I fill in "Title" with "Calendar Subspace @timestamp"
      And I fill in the following:
        | Editor              | plain_text          |
        | body[und][0][value] | Space body          |
      Then I press "edit-submit"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Calendar Section" in the "Main Region"
      And I fill in "Title" with "Calendar Subspace Section @timestamp"
      And I fill in the following:
        | Editor              | plain_text                      |
        | body[und][0][value] | Section body                    |
      Then I press "edit-submit"
    Then I should see the text "the current month and year"
      Then I click "Create Content"
      Then I click "Create Event"
      And I fill in "Title" with "Event2 @timestamp"
      And I fill in the following:
        | Editor                              | plain_text  |
        | body[und][0][value]                 | event body  |
        | field_oa_date[und][0][value][time]  | 4:30pm      |
        | field_oa_date[und][0][value2][time] | 5:30pm      |
      And I fill in "field_oa_date[und][0][value][time]" with "4:30pm"
      And I fill in "field_oa_date[und][0][value2][time]" with "5:30pm"
      Then I press "edit-submit"
    Then I should see the text "a full date of today"
      And I click "parent-dropdown"
      And I click "Calendar Space @timestamp"
      And I click "Customize this page"
      And I click "Add new pane"
      And I click "Open Atrium Events" in the "CTools modal"
      And I click to add pane "View: Open Atrium Calendar: Calendar"
      And I check the box "exposed[og_subspaces_view_all]"
      And I press "Save" in the "CTools modal"
      And I wait for the Panels IPE to deactivate
      And I press "Save as custom"
      And I wait for AJAX to finish
      And I reload the page
    Then I should see the text "4:30pEvent2 @timestamp"
    Then I should see the text "2:30pEvent @timestamp"
      And I click "parent-dropdown"
      And I click "Calendar Subspace @timestamp"
      And I click "Customize this page"
      And I click "Add new pane"
      And I click "Open Atrium Events" in the "CTools modal"
      And I click to add pane "View: Open Atrium Calendar: Calendar"
      And I check the box "exposed[og_subspaces_view_parent]"
      And I press "Save" in the "CTools modal"
      And I wait for the Panels IPE to deactivate
      And I press "Save as custom"
      And I wait for AJAX to finish
    Then I should see the text "4:30pEvent2 @timestamp"
    Then I should see the text "2:30pEvent @timestamp"
      And I click "Customize this page"
      And I click "Settings" in the "Main Region"
      And I uncheck the box "exposed[og_subspaces_view_parent]"
      And I wait for AJAX to finish
      And I press "Save" in the "CTools modal"
      And I wait for the Panels IPE to deactivate
      And I press "Save"
      And I wait for AJAX to finish
    Then I should see the text "4:30pEvent2 @timestamp"
    Then I should not see the text "2:30pEvent @timestamp"

      And I click "subspace-dropdown"
      And I click "Create new subspace"
      And I fill in "Title" with "Calendar GrandSubspace @timestamp"
      And I fill in the following:
        | Editor              | plain_text          |
        | body[und][0][value] | Space body          |
      Then I press "edit-submit"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Calendar Section" in the "Main Region"
      And I fill in "Title" with "Calendar GrandSubspace Section @timestamp"
      And I fill in the following:
        | Editor              | plain_text                      |
        | body[und][0][value] | Section body                    |
      Then I press "edit-submit"
    Then I should see the text "the current month and year"
      Then I click "Create Content"
      Then I click "Create Event"
      And I fill in "Title" with "Event3 @timestamp"
      And I fill in the following:
        | Editor                              | plain_text  |
        | body[und][0][value]                 | event body  |
        | field_oa_date[und][0][value][time]  | 10:30pm      |
        | field_oa_date[und][0][value2][time] | 11:30pm      |
      And I fill in "field_oa_date[und][0][value][time]" with "10:30pm"
      And I fill in "field_oa_date[und][0][value2][time]" with "11:30pm"
      Then I press "edit-submit"
    Then I visit the homepage in space "Calendar GrandSubspace @timestamp"
      And I click "Customize this page"
      And I click "Add new pane"
      And I click "Open Atrium Events" in the "CTools modal"
      And I click to add pane "View: Open Atrium Calendar: Calendar"
      And I check the box "exposed[og_subspaces_view_parent]"
      And I press "Save" in the "CTools modal"
      And I wait for the Panels IPE to deactivate
      And I press "Save as custom"
      And I wait for AJAX to finish
    Then I should see the text "4:30pEvent2 @timestamp"
    Then I should see the text "2:30pEvent @timestamp"
    Then I should see the text "10:30pEvent3 @timestamp"
    Then the space parent link should be "Calendar Subspace @timestamp"
    Then the space link should be "Calendar GrandSubspace @timestamp"
      And I click "Customize this page"
      And I click "Settings" in the "Main Region"
      And I uncheck the box "exposed[og_subspaces_view_parent]"
      And I wait for AJAX to finish
      And I press "Save" in the "CTools modal"
      And I wait for the Panels IPE to deactivate
      And I press "Save"
      And I wait for AJAX to finish
    Then I should not see the text "4:30pEvent2 @timestamp"
    Then I should not see the text "2:30pEvent @timestamp"
    Then I should see the text "10:30pEvent3 @timestamp"

