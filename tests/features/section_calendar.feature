Feature: Have a Calendar
  In order to have a event
  As a site administrator
  I am able to create events

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @oa_event
  Scenario: Create Calendar Section
    When I visit the homepage in space "Calendar Space @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Calendar Section" in the "Main Region"
      And I fill in "Title" with "Calendar Section @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Section body  |
      Then I press "edit-submit"
    Then I should see the text "the current month and year"
      Then I click "Create Content"
      Then I click "Create Event"
      And I fill in "Title" with "Event @timestamp"
      And I fill in the following:
        | Editor                              | plain_text  |
        | body[und][0][value]                 | event body  |
        | field_oa_date[und][0][value][time]  | 4:30pm      |
        | field_oa_date[und][0][value2][time] | 5:30pm      |
      And I fill in "field_oa_date[und][0][value][time]" with "4:30pm"
      And I fill in "field_oa_date[und][0][value2][time]" with "5:30pm"
      Then I press "edit-submit"
    Then I should see the text "a full date of today"
    Then I should see the text "4:30PM - 5:30PM"
    Then I should see the heading "Event @timestamp"
      And I click "subspace-dropdown"
      And I click "Calendar Section @timestamp" in the "Breadcrumb"
    Then I should see the text "4:30pEvent @timestamp"
      Then I log in with a member of "Calendar Space @timestamp"
      Then I visit the homepage in space "Calendar Space @timestamp"
      And I click "subspace-dropdown"
      And I click "Calendar Section @timestamp"
      Then I click "Create Content"
      Then I click "Create Event"
      And I fill in "Title" with "Event2 @timestamp"
      And I fill in the following:
        | Editor                              | plain_text  |
        | body[und][0][value]                 | event 2 body  |
        | field_oa_date[und][0][value][time]  | 4:00pm      |
        | field_oa_date[und][0][value2][time] | 5:00pm      |
      And I fill in "field_oa_date[und][0][value][time]" with "4:00pm"
      And I fill in "field_oa_date[und][0][value2][time]" with "5:00pm"
      And I fill in "field_oa_date[und][0][value][date]" with "a date of tomorrow"
      And I fill in "field_oa_date[und][0][value2][date]" with "a date of tomorrow"
      Then I press "edit-submit"
    Then I should see the text "a full date of tomorrow"
    Then I should see the text "4:00PM - 5:00PM"
    Then I should see the heading "Event2 @timestamp"
      And I click "subspace-dropdown"
      And I click "Calendar Section @timestamp" in the "Breadcrumb"
    Then I should see the text "4:30pEvent @timestamp"
    Then I should see the text "4pEvent2 @timestamp"
