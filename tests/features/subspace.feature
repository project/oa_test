Feature: Have subspaces
  In order to have a subpsaces
  As a site administrator
  I am able to create subspaces

  Background:
    Given I am logged in as a user with the "administrator" role
    And users:
    | name      | pass      | mail             | roles    |
    | TestUser  | ChangeMe! | foo@example.com  |  2       |
    | TestUser2 | ChangeMe! | foo2@example.com |  2       |
    | TestUser3 | ChangeMe! | foo2@example.com |  2       |
 

  @api @javascript @oa_space @subspaces @subspaces_public
  Scenario: Interact with multiple level
    When I go to "node/add/oa-group"
      And I fill in "Title" with "Parent Group 1 @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Group body  |
      And I select the radio button "Child's permissions - inherited users in this group's subgroups will have the permissions of a generic member of that subgroup."
      Then I press "edit-submit"
    Then I should see the heading "Parent Group 1 @timestamp"
    Then I should see the heading "Members"
    Then I should see the text "current user name" in the "Main Region"
      And I fill in "name" with "TestUser"
      And I press the "Esc" key in the "User name" field
      Then I press "Add to group"
    Then I should see the text "TestUser" in the "Main Region"
      Then I go to "/"
      And I click "subspace-dropdown"
      And I click "Create new Space"
      And I fill in "Title" with "Parent Space 1 @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Space body  |
      Then I press "edit-submit"

      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Default" in the "Main Region"
      And I fill in "Title" with "Restricted Section @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Default Section body  |
      Then I fill the select2 input for "form-item-field-oa-group-ref-und" with "Parent Group 1 @timestamp"
      And I select "Parent Group 1 @timestamp" from the dropdown results
      Then I press "edit-submit"

      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Document Section" in the "Main Region"
      And I fill in "Title" with "Document Section @timestamp"
        | Editor              | plain_text    |
        | body[und][0][value] | Document Section body  |
      Then I press "edit-submit"
  
      Then I visit the current space homepage
      Then I click "Members"
      Then I click "Inherited"
      Then I select "Parent Group 1 @timestamp" from "group_name[]"
      Then I press "Add Group"
      Then I click "Inherited"
    Then I should see the text "Parent Group 1 @timestamp" in the "Main Region"
      Then I log in with "TestUser"
      And I visit the homepage in space "Parent Space 1 @timestamp"
    Then I should see the text "Parent Space 1 @timestamp"
      Then I click "Create Content"
      Then I click "Create Document Page"
      And I fill in "Title" with "Document @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Document body  |
      Then I press "edit-submit"
    Then I should see the heading "Document @timestamp"
      And I visit content "Restricted Section @timestamp" in space "Parent Space 1 @timestamp"
    Then I should see the heading "Restricted Section @timestamp"
      Then I log in with "TestUser2"
      And I visit content "Restricted Section @timestamp" in space "Parent Space 1 @timestamp"
    Then I should not see the heading "Restricted Section @timestamp"
      And I visit the homepage in space "Parent Space 1 @timestamp"
    Then I should see the text "Parent Space 1 @timestamp"


  @api @javascript @oa_space @subspaces @subspaces_private
  Scenario: Interact with multiple level
    When I go to "node/add/oa-group"
      And I fill in "Title" with "Parent Group 1 @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Group body  |
      And I select the radio button "Child's permissions - inherited users in this group's subgroups will have the permissions of a generic member of that subgroup."
      Then I press "edit-submit"
    Then I should see the heading "Parent Group 1 @timestamp"
    Then I should see the heading "Members"
    Then I should see the text "current user name" in the "Main Region"
      And I fill in "name" with "TestUser"
      And I press the "Esc" key in the "User name" field
      Then I press "Add to group"
    Then I should see the text "TestUser" in the "Main Region"
      Then I go to "/"
      And I click "subspace-dropdown"
      And I click "Create new Space"
      And I fill in "Title" with "Parent Space 1 @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Space body  |
      And I select the radio button "Private - accessible only to space members"
      Then I press "edit-submit"

      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Default" in the "Main Region"
      And I fill in "Title" with "Restricted Section @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Default Section body  |
      Then I fill the select2 input for "form-item-field-oa-group-ref-und" with "Parent Group 1 @timestamp"
      And I select "Parent Group 1 @timestamp" from the dropdown results
      Then I press "edit-submit"
  
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Document Section" in the "Main Region"
      And I fill in "Title" with "Document Section @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Document Section body  |
      Then I press "edit-submit"
  
      Then I visit the current space homepage
      Then I click "Members"
      Then I click "Inherited"
      Then I select "Parent Group 1 @timestamp" from "group_name[]"
      Then I press "Add Group"
      Then I click "Inherited"
    Then I should see the text "Parent Group 1 @timestamp" in the "Main Region"
      Then I log in with "TestUser"
      And I visit the homepage in space "Parent Space 1 @timestamp"
    Then I should see the text "Parent Space 1 @timestamp"
      Then I click "Create Content"
      Then I click "Create Document Page"
      And I fill in "Title" with "Document @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Document body  |
      Then I press "edit-submit"
    Then I should see the heading "Document @timestamp"
      And I visit content "Restricted Section @timestamp" in space "Parent Space 1 @timestamp"
    Then I should see the heading "Restricted Section @timestamp"
      Then I log in with "TestUser2"
      And I visit content "Restricted Section @timestamp" in space "Parent Space 1 @timestamp"
    Then I should not see the heading "Restricted Section @timestamp"
    And I visit the homepage in space "Parent Space 1 @timestamp"
    Then I should not see the text "Parent Space 1 @timestamp"
