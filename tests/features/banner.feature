Feature: Have a banner image

  Background:
    Given I am logged in as a user with the "administrator" role
  
  @api @javascript @oa_appearance @oa_appearance_banner
  Scenario: Have a space
    When I visit "node/add/oa-space"
      And I fill in "Title" with "Test banner grandpa @timestamp"
      And I click "Browse"
      And I switch to the frame "mediaBrowser"
      And I attach the file "test-sm.png" to "files[upload]"
      And I press "Next"
      And I finish the file creation
      And I fill in "Banner Text" with "Test Banner Text"
      And I press "edit-submit"
    Then the space link should be "Test banner grandpa @timestamp"
    Then the banner image should be "test-sm.png"
      And I click "subspace-dropdown"
      And I click "Create new subspace"
      And I fill in "Title" with "Test banner papa @timestamp"
      And I fill in the following:
        | Editor              | plain_text          |
        | body[und][0][value] | Space body          |
      Then I press "edit-submit"
    Then the banner image should be "test-sm.png"
    Then the space parent link should be "Test banner grandpa @timestamp"
    Then the space link should be "Test banner papa @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new subspace"
      And I fill in "Title" with "Test banner child @timestamp"
      And I fill in the following:
        | Editor              | plain_text          |
        | body[und][0][value] | Space body          |
      Then I press "edit-submit"
    Then the banner image should be "test-sm.png"
    Then the space parent link should be "Test banner papa @timestamp"
    Then the space link should be "Test banner child @timestamp"
      Then I click "Edit" in the "Contextual Tabs"
      And I click "Browse"
      And I switch to the frame "mediaBrowser"
      And I attach the file "oa_splash.png" to "files[upload]"
      And I press "Next"
      And I finish the file creation
      And I press "edit-submit"
    Then the banner image should be "oa_splash.png"
    Then the banner image should not be "test-sm.png"
      Then I click "Edit" in the "Contextual Tabs"
      And I select "Hidden" from "Banner Position"
      And I press "edit-submit"
    Then the banner image should not be visible
