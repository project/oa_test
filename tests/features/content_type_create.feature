Feature: I can create new group content types


  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @content_type_create
  Scenario: I create a content type
    Given I am on "/admin/structure/types/add"
      And I fill in the following:
        | Name              | Behat Test    |
        | Description | Test content type  |
      And I press "Save content type"
    Then I should see the text "The content type Behat Test has been added."
      And I visit "/admin/config/group/fields"
      And I select "node:behat_test" from "Bundles"
      And I select "Groups audience" from "Fields"
      And I fill in "Field name" with "og_group_ref"
      And I press "Add field"
    Then I should see the text "Added field Groups audience to behat_test."
      And I select "node:behat_test" from "Bundles"
      And I select "Open Atrium Section" from "Fields"
      And I press "Add field"
    Then I should see the text "Added field Open Atrium Section to behat_test."
      And I visit the homepage in space "Test behat @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Document Section" in the "Main Region"
      And I fill in "title" with "Document Section @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Document Section body  |
      Then I press "edit-submit"
      And I visit "node/add/behat-test"
    Then the "Open Atrium Section" field should contain "NID of 'Document Section @timestamp' in 'Test behat @timestamp'"
    Then the "og_group_ref[und][0][default]" field should contain "Select2 value for space 'Test behat @timestamp'"
      And I fill in "title" with "Behat Test @timestamp"
      And I fill in the following:
        | Editor              | plain_text       |
        | body[und][0][value] | Behat test body  |
      And I press "edit-submit"
    Then I should see the heading "Behat Test @timestamp"
    Then I should see the text "Test behat @timestamp"
    Then I should see the text "Document Section @timestamp"
    Then I click "Edit" in the "Contextual Tabs"
    Then the "Open Atrium Section" field should contain "NID of 'Document Section @timestamp' in 'Test behat @timestamp'"
    Then the "og_group_ref[und][0][default]" field should contain "Select2 value for space 'Test behat @timestamp'"
