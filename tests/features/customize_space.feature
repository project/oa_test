Feature: Customize Space
  In order to have a unique space
  As a site administrator
  I am able to place widgets

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @oa_space @custimize_space
  Scenario: Add a pane to a space
    When I visit the homepage in space
      And I click "Customize this page"
      And I click "Add new pane"
      And I click "OA Admin" in the "CTools modal"
      And I wait 2 seconds
      And I click to add pane "Space Members"
      And I press "Finish" in the "CTools modal"
      And I wait for the Panels IPE to deactivate
      And I press "Save as custom"
      And I wait for AJAX to finish
      And I reload the page
    Then I should see the heading "Space administrators"
    Then I should see the link "Create New Team"