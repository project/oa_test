Feature: Have workflow

  Background:
    Given I am logged in as a user with the "administrator" role
    Given I have moderation enabled for "oa_discussion_post"

  @api @javascript @apps @oa_workbench @workbench_change_states
  Scenario: Change the states of content
    When I visit the homepage in space "Workbench Space @timestamp"
      And I click "Configure" in the "Contextual Tabs" region
      And I click "Config" in the "Configure Drop Down" region
      And I click "Workbench Moderation Profile" in the "Main Region"
      And I select "Default" from "Moderation Profile"
      And I press "Set Profile"
      And I visit "node/add/oa-discussion-post"
      And I fill in "Title" with "Discussion @timestamp"
      Then the "Moderation state" field should contain "draft"
      Then I press "Save as draft"
      Then I should see the text "Revision state: Draft"
      Then I press "Apply"
      Then I should see the text "Revision state: Needs Review"
      Then I select "Published" from "state"
      Then I press "Apply"
      Then I should see the text "Revision state: Published"
      Then I click "Unpublish this revision"
      And I select "Draft" from "Set moderation state"
      Then I press "Unpublish"
      Then I should see the text "Currently there is no published revision of this node."
      And I click "View draft"
      Then I should see the text "Revision state: Draft"

  @api @javascript @apps @oa_workbench @workbench_custom_profile
  Scenario: Change the states of content
    When I visit "admin/config/workbench/moderation"
      Then I fill in "New state" with "State @timestamp"
      Then I fill in "states[0][description]" with "Description @timestamp"
      Then I press "Save"
      Then I visit "admin/config/workbench/moderation/transitions"
      Then I fill in "Transition Name" with "Transition @timestamp"
      Then I select "Draft" from "New transition"
      Then I select "State @timestamp" from "transitions[new][to_name]"
      Then I press "Save"
      Then I visit "admin/config/workbench/moderation/profile"
      Then I click "Add workbench moderation profile"
      Then I fill in "Label" with "Profile @timestamp"
      Then I select "Submit for Review (Draft --> Needs Review)" from "field_transitions[und][]"
      Then I additionally select "Transition @timestamp (Draft --> State @timestamp)" from "field_transitions[und][]"
      Then I additionally select "Publish (Needs Review --> Published)" from "field_transitions[und][]"
      Then I press "Save profile"
      Then I should see the text "Machine name: profile_@timestamp"
    When I visit the homepage in space "Workbench Space @timestamp"
      And I click "Configure" in the "Contextual Tabs" region
      And I click "Config" in the "Configure Drop Down" region
      And I click "Workbench Moderation Profile" in the "Main Region"
      And I select "Profile @timestamp" from "Moderation Profile"
      And I press "Set Profile"
      And I visit "node/add/oa-discussion-post"
      And I fill in "Title" with "Discussion @timestamp"
      Then the "Moderation state" field should contain "draft"
      Then I select "State @timestamp" from "Moderation state"
      Then I press "Save as draft"
      Then I should see the text "Revision state: State @timestamp"