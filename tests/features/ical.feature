Feature: Have a Calendar with Ical

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @oa_event @oa_event_ical
  Scenario: Visit ical
    When I visit the homepage in space "Calendar Space @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Calendar Section" in the "Main Region"
      And I fill in "Title" with "Calendar Section @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Section body  |
      Then I press "edit-submit"
    Then I should see the text "the current month and year"
    And I try to download the ical feed
    Then the download response should not contain "event body"
      Then I click "Create Content"
      Then I click "Create Event"
      And I fill in "Title" with "Event @timestamp"
      And I fill in the following:
        | Editor                              | plain_text  |
        | body[und][0][value]                 | event body  |
        | field_oa_date[und][0][value][time]  | 4:30pm      |
        | field_oa_date[und][0][value2][time] | 5:30pm      |
      And I fill in "field_oa_date[und][0][value][time]" with "4:30pm"
      And I fill in "field_oa_date[und][0][value2][time]" with "5:30pm"
      Then I press "edit-submit"
    Then I should see the text "a full date of today"
    Then I should see the text "4:30PM - 5:30PM"
    Then I should see the heading "Event @timestamp"
      And I click "subspace-dropdown"
      And I click "Calendar Section @timestamp" in the "Breadcrumb"
    Then I should see the text "4:30pEvent @timestamp"
    And I try to download the ical feed
    Then the download response should contain "event body"