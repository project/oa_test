Feature: Have a Document Section
  In order to have a news item
  As a site administrator
  I am able to create news items

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @oa_news
  Scenario: Create Document Section Section
    When I visit the homepage in space "News Space @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Document Section" in the "Main Region"
      And I fill in "title" with "Document Section @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Document Section body  |
      Then I press "edit-submit"
      Then I click "Create Content"
      Then I click "Create Document"
      And I fill in "Title" with "News @timestamp"
      And I fill in the following:
        | Editor                              | plain_text |
        | body[und][0][value]                 | news body  |
      Then I press "edit-submit"
      And I click "subspace-dropdown"
      And I click "Document Section @timestamp" in the "Breadcrumb"
    Then I should see the text "News @timestamp"
    Then I should see the text "Document Section body"
      Then I log in with a member of "News Space @timestamp"
      Then I visit the homepage in space "News Space @timestamp"
      And I click "subspace-dropdown"
      And I click "Document Section @timestamp"
      Then I click "Create Content"
      Then I click "Create Document"
      And I fill in "Title" with "News2 @timestamp"
      And I fill in the following:
        | Editor                              | plain_text |
        | body[und][0][value]                 | news 2 body  |
      Then I press "edit-submit"
      And I click "subspace-dropdown"
      And I click "Document Section @timestamp" in the "Breadcrumb"
    Then I should see the text "News @timestamp"
    Then I should see the text "News2 @timestamp"
    Then I should see the text "news 2 body"
