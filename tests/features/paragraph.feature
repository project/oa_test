Feature: Have a Document with paragraphs
  In order to have dynamic document
  As a site administrator
  I am able to add paragraphs

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @oa_related @text_paragraph
  Scenario: Create document with text paragraph
    When I visit the homepage in space "Paragraph Test Space 1 @timestamp"
      And I visit "node/add/oa-wiki-page"
      And I fill in "Title" with "My document with paragraphs 1 @timestamp"
      And I press the "Add Text" button
      And I wait for AJAX to finish
      And I fill in "field_oa_related[und][0][field_paragraph_text][und][0][format]" with "plain_text"
      And I fill in "field_oa_related[und][0][field_paragraph_text][und][0][value]" with "Text paragraph content, what a walrus"
      And I press "Publish"
    Then I should see the text "Text paragraph content, what a walrus"
      Then I fill in "search_text" with "Walrus"
      And I press "Search Button"
    Then I should see the text "My document with paragraphs 1 @timestamp"
    Then I should see the heading "Search"

  @api @javascript @oa_related @snippet_paragraph
  Scenario: Create document with snippet paragraph
    When I visit the homepage in space "Paragraph Test Space 2 @timestamp"
      And I visit "node/add/oa-wiki-page"
      And I fill in "Title" with "Document to Reference 2 @timestamp"
      And I fill in the following:
        | Editor              | plain_text                |
        | body[und][0][value] | A unicorn went a walking. |
    Then I press "edit-submit"
      And I visit "node/add/oa-wiki-page"
      And I fill in "Title" with "My document with paragraphs 2 @timestamp"
      And I press the "Add Snippet" button
      And I wait for AJAX to finish
      And I fill in "field_oa_related[und][0][field_snippet_content][und][0][target_id]" with "Document to Reference 2 @timestamp"
      And I wait for 4 seconds
      And I wait for AJAX to finish
      And I select "Document to Reference 2 @timestamp" from the dropdown results
      And I press the "Esc" key in the "field_oa_related[und][0][field_snippet_content][und][0][target_id]" field
      And I check the box "Lock revision"
      And I press "Publish"
    Then I should see the text "A unicorn went a walking"
    Then I should not see the text "Document to Reference 2 @timestamp"
      Then I click "Edit" in the "Contextual Tabs"
      Then I check the box "Show Title"
      Then I press "edit-submit"
    Then I should see the text "A unicorn went a walking"
    Then I should see the text "Document to Reference 2 @timestamp"
      Then I click "Edit" in the "Contextual Tabs"
      Then I fill in "Title Override" with "Prancing Horses"
      And I wait for AJAX to finish
      Then I wait for 2 seconds
      Then I press "edit-submit"
    Then I should see the text "A unicorn went a walking"
    Then I should not see the text "Document to Reference 2 @timestamp"
    Then I should see the text "Prancing Horses"
      Then I visit content "Document to Reference 2 @timestamp" in space "Paragraph Test Space 2 @timestamp"
      Then I click "Edit" in the "Contextual Tabs"
      And I fill in "body[und][0][value]" with "A purple pineapple ate the dog."
      And I press "edit-submit"
      Then I visit content "My document with paragraphs 2 @timestamp" in space "Paragraph Test Space 2 @timestamp"
    Then I should see the text "A unicorn went a walking"
    Then I should not see the text "A purple pineapple ate the dog."
      Then I click "Edit" in the "Contextual Tabs"
      And I uncheck the box "Lock revision"
      And I press "edit-submit"
    Then I should not see the text "A unicorn went a walking"
    Then I should see the text "A purple pineapple ate the dog."


  @api @javascript @oa_related @related_paragraph
  Scenario: Create document with related paragraph
    When I visit the homepage in space "Paragraph Test Space 3 @timestamp"
      And I visit "node/add/oa-event"
      And I fill in "Title" with "Event to Reference @timestamp"
      And I fill in the following:
        | Editor                           | plain_text                                  |
        | body[und][0][value]              | A boy went to town.                         |
        | field_oa_address[und][0][format] | plain_text |
        | field_oa_address[und][0][value]  | 1347 Folsom Street, San Francisco, CA 94103 |
      Then I press "edit-submit"
    Then I should see the text "Event to Reference @timestamp"
    Then I should see the text "1347 Folsom Street, San Francisco, CA 94103"
    Then I should see the text "A boy went to town"
      And I visit "node/add/oa-wiki-page"
      And I fill in "Title" with "My document with paragraphs 3 @timestamp"
      And I press the "Add Related Content" button
      And I wait for AJAX to finish
        And I fill in "field_oa_related[und][0][field_oa_related_content][und][0][target_id]" with "Event to Reference @timestamp"
      And I wait for 4 seconds
      And I wait for AJAX to finish
      And I select "Event to Reference @timestamp" from the dropdown results
      And I press the "Esc" key in the "field_oa_related[und][0][field_oa_related_content][und][0][target_id]" field
      And I select "Full content" from "field_oa_related[und][0][field_oa_content_layout][und]"
      And I wait for AJAX to finish
      And I press "Publish"
    Then I should see the text "Event to Reference @timestamp"
    Then I should see the text "1347 Folsom Street, San Francisco, CA 94103"
    Then I should see the text "A boy went to town"
    Then I should see the text "RELATED EVENT"
      Then I visit content "Event to Reference @timestamp" in space "Paragraph Test Space 3 @timestamp"
      Then I click "Edit" in the "Contextual Tabs"
        And I press the "Add Text" button
        And I wait for AJAX to finish
        And I fill in "field_oa_related[und][0][field_paragraph_text][und][0][format]" with "plain_text"
        And I fill in "field_oa_related[und][0][field_paragraph_text][und][0][value]" with "Text paragraph content, what a walrus"
        And I press "edit-submit"
    Then I should see the text "what a walrus"
    Then I visit content "My document with paragraphs 3 @timestamp" in space "Paragraph Test Space 3 @timestamp"
    Then I should see the text "Event to Reference @timestamp"
    Then I should see the text "1347 Folsom Street, San Francisco, CA 94103"
    Then I should see the text "A boy went to town"
    Then I should see the text "RELATED EVENT"
    Then I should see the text "what a walrus"


  @api @javascript @oa_related @media_paragraph
  Scenario: Create document with media paragraph
    When I visit the homepage in space "Paragraph Test Space 4 @timestamp"
      And I visit "node/add/oa-wiki-page"
      And I fill in "Title" with "My document with paragraphs 4 @timestamp"
      And I press the "Add Media Gallery" button
      And I wait for AJAX to finish
      And I click "Browse"
      And I wait for AJAX to finish
      And I switch to the frame "mediaBrowser"
      And I attach the file "oa_splash.png" via plupload
      And I finish the image creation
      And I press "edit-submit"
    Then I should see the image "oa_splash.png"


  @api @javascript @oa_related @all_paragraph
  Scenario: Create document with all paragraphs
    When I visit the homepage in space "Paragraph Test Space 5 @timestamp"
      And I visit "node/add/oa-event"
      And I fill in "Title" with "Event to Reference @timestamp"
      And I fill in the following:
        | Editor                           | plain_text                                  |
        | body[und][0][value]              | A boy went to town.                         |
        | field_oa_address[und][0][format] | plain_text |
        | field_oa_address[und][0][value]  | 1347 Folsom Street, San Francisco, CA 94103 |
      Then I press "edit-submit"
      And I visit "node/add/oa-wiki-page"
      And I fill in "Title" with "Document to Reference 3 @timestamp"
      And I fill in the following:
        | Editor              | plain_text                |
        | body[und][0][value] | A unicorn went a walking. |
    Then I press "edit-submit"
      And I visit "node/add/oa-wiki-page"
      And I fill in "Title" with "My document with paragraphs 5 @timestamp"
      And I press the "Add Media Gallery" button
      And I wait for AJAX to finish
      And I click "Browse"
      And I wait for AJAX to finish
      And I switch to the frame "mediaBrowser"
      And I attach the file "oa_splash.png" via plupload
      And I finish the image creation
      And I wait for AJAX to finish
      And I press the "Add Related Content" button
      And I wait for AJAX to finish
      And I fill in "field_oa_related[und][1][field_oa_related_content][und][0][target_id]" with "Event to Reference @timestamp"
      And I wait for 4 seconds
      And I wait for AJAX to finish
      And I select "Event to Reference @timestamp" from the dropdown results
      And I press the "Esc" key in the "field_oa_related[und][1][field_oa_related_content][und][0][target_id]" field
      And I select "Full content" from "field_oa_related[und][1][field_oa_content_layout][und]"
      And I wait for AJAX to finish
      And I press the "Add Snippet" button
      And I wait for AJAX to finish
      And I fill in "field_oa_related[und][2][field_snippet_content][und][0][target_id]" with "Document to Reference 3 @timestamp"
      And I wait for 4 seconds
      And I wait for AJAX to finish
      And I select "Document to Reference 3 @timestamp" from the dropdown results
      And I press the "Esc" key in the "field_oa_related[und][2][field_snippet_content][und][0][target_id]" field
      And I press the "Add Text" button
      And I wait for AJAX to finish
      And I fill in "field_oa_related[und][3][field_paragraph_text][und][0][format]" with "plain_text"
      And I fill in "field_oa_related[und][3][field_paragraph_text][und][0][value]" with "Text paragraph content, what a walrus"
      And I press "edit-submit"
    Then I should see the image "oa_splash.png"
    Then I should see the text "Event to Reference @timestamp"
    Then I should see the text "1347 Folsom Street, San Francisco, CA 94103"
    Then I should see the text "A boy went to town"
    Then I should see the text "RELATED EVENT"
    Then I should see the text "A unicorn went a walking"
    Then I should not see the text "Document to Reference 3 @timestamp"
    Then I should see the text "My document with paragraphs 5 @timestamp"
    Then I should see the text "Text paragraph content, what a walrus"
