Feature: Add comments
  In order to have comments
  As a site administrator
  I can comment

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @oa_comment @oa_comment_discussion
  Scenario: Comment on discussion
    When comments are enabled for "Discussion Post"
      And I visit the homepage in space "Comment Space @timestamp"
      And I visit "node/add/oa-discussion-post"
    Then I should see the heading "Comment options"
      And I fill in "Title" with "My discussion post with comments @timestamp"
      And I press "Publish"
    Then I should see the heading "My discussion post with comments @timestamp"
    Then I should see the heading "New Reply"
      And I fill in the following:
        | comment_body[und][0][format] | plain_text      |
        | comment_body[und][0][value]  | Comment Text!!  |
      And I press the "Add Text" button
      And I wait for AJAX to finish
      And I fill in "field_oa_related[und][0][field_paragraph_text][und][0][format]" with "plain_text"
      And I fill in "field_oa_related[und][0][field_paragraph_text][und][0][value]" with "Text paragraph content, what a walrus"
      And I press "Save" in the "Comment Reply Buttons"
    Then I should see the text "Expand All"
    Then I should see the text "Comments"
    Then I should see the text "Comment Text!!" for comment "1"
    Then I should see the text "Text paragraph content, what a walrus" for comment "1"

      And I fill in the following:
        | comment_body[und][0][format] | plain_text      |
        | comment_body[und][0][value]  | Comment Text!!  |
      And I press the "Add Media Gallery" button
      And I wait for AJAX to finish
      And I click "Browse"
      And I wait for AJAX to finish
      And I switch to the frame "mediaBrowser"
      And I attach the file "oa_splash.png" via plupload
      And I finish the image creation
      And I press "Save" in the "Comment Reply Buttons"
    Then I should see the image "oa_splash.png"

    When comments are disabled for "Discussion Post"
      And I visit "node/add/oa-discussion-post"
    Then I should not see the heading "Comment options"
