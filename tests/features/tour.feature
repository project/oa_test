Feature: Take tour

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @oa_tour @oa_tour_banner
  Scenario: Take banner tour
    When I visit ""
      And I click "tours-dropdown"
      And I click "Banners" in the "Tours" region
      Then I should see the heading "Create Default Space"
      And I fill in "Title" with "Tour Banner Space @timestamp"


  @api @javascript @oa_tour @oa_tour_group
  Scenario: Take group tour
    When I visit ""
      And I click "tours-dropdown"
      And I click "Groups" in the "Tours" region
      Then I should see "Creating a Group" in the "Tour Popup"
      And I press "Next »"
      Then I should see the heading "Add content"
      And I press "Next »"
      Then I should see the heading "Create Group"
      Then I should see the text "Title" in the "Tour Popup" region
      And I fill in "Title" with "Tour Groups Group @timestamp"
      And I press "Next »"
      Then I should see the text "Description" in the "Tour Popup"
      And I press "Next »"
      Then I should see the text "Permalink" in the "Tour Popup"
      And I press "Next »"
      Then I should see the text "Featured Image" in the "Tour Popup"
      And I press "Next »"
      Then I should see the text "Group Visibility" in the "Tour Popup"
      And I press "Next »"
      Then I should see the text "Save!" in the "Tour Popup" region
      And I press "End Tour"
      And I press "Publish"
      Then I should see the heading "Tour Groups Group @timestamp"

  @api @javascript @oa_tour @oa_tour_menu
  Scenario: Take menu tour
    When I visit the homepage in space "Menu Tour @timestamp"
    And I visit ""
      And I click "tours-dropdown"
      And I click "Menus" in the "Tours" region
      Then I should see the text "Main Menu" in the "Tour Popup" region
      And I press "Next »"
      Then I should see the heading "Config"
      Then I should see the text "Show main menu"
      Then I should see the text "Show Main Menu" in the "Tour Popup" region
      And I press "Next »"
      Then I should see the text "Adding Items to Menu" in the "Tour Popup" region
      Then I should see the heading "Create Default Space"
      And I press "Next »"
      Then I should see the text "Space Menus" in the "Tour Popup" region
      Then I should see the heading "Menu Tour @timestamp"
      And I press "Next »"
      Then I should see the text "Space Menu" in the "Tour Popup" region
      Then I should see the link "Menu" in the "Main Region"
      And I press "Next »"
      Then I should see the text "Space Menu" in the "Tour Popup" region
      Then I should see the link "Add link" in the "Main Region"

  @api @javascript @oa_tour @oa_tour_user_create
  Scenario: Take user create tour
    When I visit ""
      And I click "tours-dropdown"
      And I click "User Creation" in the "Tours" region
      Then I should see the text "Creating a User" in the "Tour Popup" region
      And I press "Next »"
      Then I should see the text "Add User" in the "Tour Popup" region
      Then I click "Add user"
      Then I should see the text "Username" in the "Tour Popup" region
      Then I fill in "Username" with "Test User @timestamp"
      And I press "Next »"
      Then I should see the text "Email" in the "Tour Popup" region
      Then I fill in "E-mail address" with "example+@timestamp@example.com"
      And I press "Next »"
      Then I should see the text "Password" in the "Tour Popup" region
      Then I fill in "pass[pass1]" with "testpass @timestamp"
      Then I fill in "pass[pass2]" with "testpass @timestamp"
      And I press "Next »"
      Then I should see the text "Display Name" in the "Tour Popup" region
      Then I fill in "Display Name" with "Display Name @timestamp"
      And I press "Next »"
      Then I should see the text "Submit" in the "Tour Popup" region
      Then I press "Create new account"
      Then I should see the text "Created a new user account for Test User @timestamp. No e-mail has been sent."

  @api @javascript @oa_tour @oa_tour_toolbar
  Scenario: Take toolbar tour
    When I visit the homepage in space "Toolbar Tour @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Document Section" in the "Main Region"
      And I fill in "title" with "Document Section @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Document Section body  |
      Then I press "edit-submit"
    And I visit ""
      And I click "tours-dropdown"
      And I click "Toolbar" in the "Tours" region
      Then I should see the text "Toolbar" in the "Tour Popup" region
      And I press "Next »"
      Then I should see the text "Favorites" in the "Tour Popup" region
      And I press "Next »"
      Then I should see the text "Search" in the "Tour Popup" region
      And I press "Next »"
      Then I should see the text "User Profile" in the "Tour Popup" region
      And I press "Next »"
      Then I should see the text "Breadcrumbs" in the "Tour Popup" region
      Then I should see the link "Spaces" in the "Breadcrumb"
      And I press "Next »"
      Then I should see the text "Breadcrumbs Shift" in the "Tour Popup" region
      Then I should see the heading "Toolbar Tour @timestamp"
      Then I should see the link "Toolbar Tour @timestamp" in the "Breadcrumb"
      And I press "Next »"
      Then I should see the text "Adding Content" in the "Tour Popup" region
      And I press "End Tour"
      Then I click "Create Content"
      Then I click "Create Document"

  @api @javascript @oa_tour @oa_tour_tour
  Scenario: Take tour tour
    When I visit ""
      And I click "tours-dropdown"
      And I click "Site Tours" in the "Tours" region
      Then I should see the text "Site Tours" in the "Tour Popup" region
      And I press "Next »"
      Then I should see the text "Editing Tours" in the "Tour Popup" region
      And I press "Next »"
      Then I should see the heading "Site Tours"
      Then I click "Add site tour"
      Then I should see the text "Creating a Tour" in the "Tour Popup" region
      Then I fill in "title" with "Behat Tour @timestamp"
      And I press "Next »"
      Then I should see the text "Add Steps" in the "Tour Popup" region
      Then I fill in "Start Path" with "<front>"
      Then I press "Add new step"
      Then I fill in "Popup Title" with "Step 1 @timestamp"
      And I fill in the following:
        | Editor        | plain_text    |
        | Popup Content | Popup Content |
      Then I press "Create step"
      Then I press "End Tour"
      Then I press "Save Tour"
      Then I visit ""
      And I click "tours-dropdown"
      And I click "Behat Tour @timestamp" in the "Tours" region
      Then I should see the text "Step 1 @timestamp" in the "Tour Popup" region


  @api @javascript @oa_tour @oa_tour_home
  Scenario: Take home tour
    When I visit the homepage in space "HomePage Tour @timestamp"
    And I visit ""
      And I click "tours-dropdown"
      And I click "Setting Your Home" in the "Tours" region
      Then I should see the text "Setting Your Home" in the "Tour Popup" region
      And I press "Next »"
      Then I should see the text "Select Your Default Home" in the "Tour Popup" region
      Then I press "End Tour"
      Then I store the "site_frontpage" variable
      Then I fill in "site_frontpage" with "Path for space 'HomePage Tour @timestamp'"
      Then I press "Save configuration"
      Then I visit ""
      Then I should see the heading "HomePage Tour @timestamp"

  @api @javascript @oa_tour @oa_tour_sandbox
  Scenario: Take sandbox tour
    When I visit the homepage in space "Sandbox Tour @timestamp"
      And I click "subspace-dropdown"
      And I click "Create new section"
      And I click "Document Section" in the "Main Region"
      And I fill in "title" with "Document Section @timestamp"
      And I fill in the following:
        | Editor              | plain_text    |
        | body[und][0][value] | Document Section body  |
      Then I press "edit-submit"
    And I visit ""
      And I click "tours-dropdown"
      And I click "Sandbox Mode" in the "Tours" region
      Then I should see the text "Sandbox Mode" in the "Tour Popup" region
      And I press "Next »"
      Then I should see the text "Enable Sandbox" in the "Tour Popup" region
      And I check the box "Activate sandbox mode?"
      And I press "Save"
      And I press "Next »"
      Then I should see the link "Click this to toggle sandbox mode."
      Then I should see the text "Sandbox Indicator" in the "Tour Popup" region
      Then I click "Click this to toggle sandbox mode."
      Then I should see the text "Sandbox Indicator" in the "Tour Popup" region
      And I press "Next »"
      Then I should see the text "Delete Sandbox" in the "Tour Popup" region
      And I press "Next »"
      Then I should see the text "Delete Content" in the "Tour Popup" region
      And I press "Next »"
      Then I should see the text "Creating Sandbox Content" in the "Tour Popup" region
      And I press "End Tour"
      And I fill in "Title" with "News @timestamp"
      And I fill in the following:
        | Editor                              | plain_text |
        | body[und][0][value]                 | news body  |
      Then I press "edit-submit"
      Then content "content 'News @timestamp' in 'Sandbox Tour @timestamp'" should exist
      Then I click "Click this to toggle sandbox mode."
      Then I click "Delete test content"
      Then I press "Delete"
      Then content "content 'News @timestamp' in 'Sandbox Tour @timestamp'" should not exist
