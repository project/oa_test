Feature: Clone a document with paragraphs
  In order to have good cloning experience
  As a site administrator
  I am able to clone a document with paragraphs

  Background:
    Given I am logged in as a user with the "administrator" role

  @api @javascript @oa_related @clone_paragraph
  Scenario: Create document with text paragraph
    When I visit the homepage in space "Paragraph Clone @timestamp"
      And I visit "node/add/oa-wiki-page"
      And I fill in "Title" with "My clonable document with paragraphs @timestamp"
      And I press the "edit-field-oa-related-und-add-more-add-more-bundle-paragraph-text" button
      And I wait for AJAX to finish
      And I fill in "field_oa_related[und][0][field_paragraph_text][und][0][format]" with "plain_text"
      And I fill in "field_oa_related[und][0][field_paragraph_text][und][0][value]" with "Text paragraph content, what a walrus"
      And I press "Publish"
    Then I should see the text "Text paragraph content, what a walrus"
    When I visit the homepage in space "Paragraph Clone @timestamp"
      And I click "Configure" in the "Contextual Tabs" region
      And I click "Clone this space"
    Then I press "edit-submit"
      And I wait for AJAX to finish
      And I visit content "My clonable document with paragraphs @timestamp" in space "Paragraph Clone @timestamp"
    Then I click "Edit" in the "Contextual Tabs"
      And I fill in "field_oa_related[und][0][field_paragraph_text][und][0][value]" with "New Text about something else."
      And I press "edit-submit"
    Then I should see the text "New Text about something else."
    Then I should not see the text "Text paragraph content, what a walrus"
      And I visit content "My clonable document with paragraphs @timestamp" in space "Clone of Paragraph Clone @timestamp"
    Then I should not see the text "New Text about something else."
    Then I should see the text "Text paragraph content, what a walrus"
