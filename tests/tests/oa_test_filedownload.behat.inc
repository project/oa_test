<?php
/**
 * @file
 * Provide Behat step-definitions for file download in OA.
 *
 */

use Drupal\DrupalExtension\Context\DrupalSubContextInterface;
use Behat\Behat\Context\BehatContext;
use Drupal\Component\Utility\Random;
use Behat\Behat\Event\StepEvent;
use Behat\Mink\Exception\ElementNotFoundException;

class TestOaFileDownloadSubContext extends BehatContext implements DrupalSubContextInterface {
  /**
   * Initializes context.
   */
  public function __construct(array $parameters = array()) {
  }

  public static function getAlias() {
    return 'testoafiledownload';
  }

  /**
   * Get the session from the parent context.
   */
  protected function getSession() {
    return $this->getMainContext()->getSession();
  }

  /**
   * @When /^I try to download "([^"]*)"$/
   *
   * via https://www.jverdeyen.be/php/behat-file-downloads/
   */
  public function iTryToDownload($url) {

     $client = new \Guzzle\Http\Client($this->getSession()->getCurrentUrl());
     $request = $client->get($url);
     $this->response = $request->send();
     $this->response_body = (string) $this->response->getBody();
  }

  /**
   * @Then /^the download response should not contain "([^"]*)"$/
   */
  public function theDownloadResponseShouldNotContain($arg1) {
    if (!empty($this->response_body) && strpos($this->response_body, $arg1) !== FALSE) {
      throw new \Exception(sprintf('Response body contains %s on %s', $arg1, $this->getSession()->getCurrentUrl()));
    }
  }

  /**
   * @Then /^the download response should contain "([^"]*)"$/
   */
  public function theDownloadResponseShouldContain($arg1) {
    if (!empty($this->response_body) && strpos($this->response_body, $arg1) === FALSE) {
      throw new \Exception(sprintf('Response body does not contain %s on %s', $arg1, $this->getSession()->getCurrentUrl()));
    }
  }

  /**
   * @When /^I try to download the ical feed$/
   *
   * via https://www.jverdeyen.be/php/behat-file-downloads/
   */
   public function iTryToDownloadTheIcalFeed() {
    $driver = $this->getSession()->getDriver();

    $link = $driver->find("//a[contains(concat(' ', @class, ' '), ' ical-icon ')]");
    if ($link) {
      $this->iTryToDownload($link[0]->getAttribute('href'));
    }
    else {
      throw new \Exception(sprintf('Unable to find the ical icon on %s', $app_title, $this->getSession()->getCurrentUrl()));
    }
  }

   /**
   * @Then /^I should see response status code "([^"]*)"$/
   */
  public function iShouldSeeResponseStatusCode($statusCode){
    $responseStatusCode = $this->response->getStatusCode();

    if (!$responseStatusCode == intval($statusCode)) {
      throw new \Exception(sprintf("Did not see response status code %s, but %s.", $statusCode, $responseStatusCode));
    }
  }

   /**
   * @Then /^I should see in the header "([^"]*)":"([^"]*)"$/
   */
   public function iShouldSeeInTheHeader($header, $value) {
     $headers = $this->response->getHeaders();
     if ($headers->get($header) != $value) {
        throw new \Exception(sprintf("Did not see %s with value %s.", $header, $value));
     }
   }
}