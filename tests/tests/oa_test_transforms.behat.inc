<?php
/**
 * @file
 * Provide Behat step-definitions for text transformation.
 *
 */

use Drupal\DrupalExtension\Context\DrupalSubContextInterface;
use Behat\Behat\Context\BehatContext;
use Drupal\Component\Utility\Random;
use Behat\Behat\Event\StepEvent;
use Behat\Mink\Exception\ElementNotFoundException;

class TestOaTransformsSubContext extends BehatContext implements DrupalSubContextInterface {

  /**
   * Initializes context.
   */
  public function __construct(array $parameters = array()) {
  }

  public static function getAlias() {
    return 'testoatransform';
  }

  /**
   * Get the session from the parent context.
   */
  protected function getSession() {
    return $this->getMainContext()->getSession();
  }

  protected $dateFormat = 'M j Y';
  protected $dateFormatFull = 'l, M j, Y';

  /**
   * Transforms relative date statements compatible with strtotime().
   * Example: "date 2 days ago" might return "2013-10-10" if its currently
   * the 12th of October 2013. Customize through {@link setDateFormat()}.
   * @see http://chillu.tumblr.com/post/67056088859/behat-step-transformations-for-relative-time
   *
   * @Transform /^(?:(the|a)) date of ([^"]*)$/
   */
  public function castRelativeToAbsoluteDate($prefix, $val) {
    $timestamp = strtotime($val);
    if (!$timestamp) {
      throw new \InvalidArgumentException(sprintf(
        "Can't resolve '%s' into a valid datetime value",
        $val
      ));
    }
    return date($this->dateFormat, $timestamp);
  }
  /**
   * Do full date for display of date.
   *
   * @Transform /^(?:(the|a)) full date of ([^"]*)$/
   */
  public function castRelativeToFullAbsoluteDate($prefix, $val) {
    $timestamp = strtotime($val);
    if (!$timestamp) {
      throw new \InvalidArgumentException(sprintf(
        "Can't resolve '%s' into a valid datetime value",
        $val
      ));
    }
    return date($this->dateFormatFull, $timestamp);
  }

  public function getDateFormat() {
    return $this->dateFormat;
  }

  public function setDateFormat($format) {
    $this->dateFormat = $format;
  }

  /**
   * Gets the current month
   *
   * @Transform /^the current month$/
   */
  public function getTheCurrentMonth() {
    return date("F");
  }

  /**
   * Gets the current month and year.
   *
   * @Transform /^the current month and year$/
   */
  public function getTheCurrentMonthAndYear() {
    return date("F Y");
  }

  /**
   * Gets the current month and year.
   *
   * @Transform /^Random Text$/
   */
  public function getRandomText() {
    $random = new Random();
    return $random->name(8);
  }

  /**
   * Gets the current user's name
   *
   * @Transform /^current user name$/
   */
  public function getCurrentUsersName() {
    return $this->getMainContext()->user->name;
  }
}
