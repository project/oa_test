<?php
/**
 * @file
 * Provide Behat step-definitions for navigation views in OA.
 *
 */

use Drupal\DrupalExtension\Context\DrupalSubContextInterface;
use Behat\Behat\Context\BehatContext;
use Drupal\Component\Utility\Random;
use Behat\Behat\Event\StepEvent;
use Behat\Mink\Exception\ElementNotFoundException;

class TestOaViewsSubContext extends BehatContext implements DrupalSubContextInterface {
  /**
   * Initializes context.
   */
  public function __construct(array $parameters = array()) {
  }

  public static function getAlias() {
    return 'testoaviews';
  }

  /**
   * Get the session from the parent context.
   */
  protected function getSession() {
    return $this->getMainContext()->getSession();
  }

  /**
   * @Then /^I click to add a views display$/
   */
  public function iClickToAddAViewsDisplay() {
    $driver = $this->getSession()->getDriver();
    $links = $driver->find("//li[contains(concat(' ', @class, ' '), ' add ')]/a[contains(normalize-space(text()), 'Add')]");
    if ($links) {
      $links[0]->click();
    }
    else {
      throw new \Exception(sprintf('Unable to add link for view on %s', $this->getSession()->getCurrentUrl()));
    }
  }

  /**
   * @Then /^I click add for views "([^"]*)"$/
   */
  public function iClickAddForViews($view_section) {
    $driver = $this->getSession()->getDriver();
    $class = strtolower(str_replace(' ', '-', $view_section));
    $links = $driver->find("//div[contains(concat(' ', @class, ' '), ' " . $class ." ')]//a[contains(normalize-space(text()), 'Add')]");
    if ($links) {
      $links[0]->click();
    }
    else {
      throw new \Exception(sprintf('Unable to add link for %s on %s', $view_section, $this->getSession()->getCurrentUrl()));
    }
  }

  /**
   * @Then /^I expose the form$/
   */
  public function iExposeTheForm() {
    $driver = $this->getSession()->getDriver();
    $checkboxes = $driver->find("//input[@id='edit-options-expose-button-checkbox-checkbox']");
    if ($checkboxes) {
      $checkboxes[0]->click();
    }
    else {
      throw new \Exception(sprintf('Unable to find expose form checkbox on %s', $this->getSession()->getCurrentUrl()));
    }
  }

  /**
   * @Then /^I check the views checkbox "([^"]*)"$/
   */
  public function iCheckTheViewsCheckbox($id) {
    $driver = $this->getSession()->getDriver();
    // However behat currently checks it, ajax doesn't respond.
    $checkbox = $driver->find("//input[@id='" . $id . "']");
    if ($checkbox) {
      $checkbox[0]->click();
    }
    else {
      throw new \Exception(sprintf('Unable to find the checkbox %s on the page %s', $id, $this->getSession()->getCurrentUrl()));
    }
  }

  /**
   * @Given /^I click "(?P<link>(?:[^"]|\\")*)" on row "([^"]*)" of view "([^"]*)" "([^"]*)"$/
   * @Given /^I click "(?P<link>(?:[^"]|\\")*)" on row "([^"]*)" of view "([^"]*)"$/
   */
  public function iClickForViewsRow($link, $row_number, $view_name, $display_id = NULL) {
    $driver = $this->getSession()->getDriver();
    $class = "view-id-$view_name";
    if ($display_id) {
      $class .= " view-display-id-$display_id";
    }
    $links = $driver->find("//div[contains(concat(' ', @class, ' '), ' " . $class . " ')]//*[contains(concat(' ', @class, ' '), ' views-row-" . $row_number . " ')]");
    if ($links) {
      $links[0]->clickLink($link);
    }
    else {
      throw new \Exception(sprintf('Unable to find the row %s for the view %s on the page %s', $row_number, $view_name, $this->getSession()->getCurrentUrl()));
    }
  }

  /**
   * @Then /^I click on a the calendar for "([^"]*)"$/
   */
  public function iClickOnATheCalendarFor($date) {
    $dateFormat = 'Y-m-d';
    $timestamp = strtotime($date);
    if (!$timestamp) {
      throw new \InvalidArgumentException(sprintf(
        "Can't resolve '%s' into a valid datetime value",
        $date
      ));
    }
    $formated_date = date($dateFormat, $timestamp);
    $driver = $this->getSession()->getDriver();
    $cells = $driver->find("//td[@data-date='" . $formated_date . "']");
    if ($cells) {
      $cells[0]->click();
    }
    else {
      throw new \Exception(sprintf('Unable to find the cell with date %s (%s) on the page %s', $date, $formated_date, $this->getSession()->getCurrentUrl()));
    }
  }
}