<?php
/**
 * @file
 * Provide Behat step-definitions for files/media in OA.
 *
 */

use Drupal\DrupalExtension\Context\DrupalSubContextInterface;
use Behat\Behat\Context\BehatContext;
use Drupal\Component\Utility\Random;
use Behat\Behat\Event\StepEvent;
use Behat\Mink\Exception\ElementNotFoundException;

class TestOaFilesSubContext extends BehatContext implements DrupalSubContextInterface {
  /**
   * Initializes context.
   */
  public function __construct(array $parameters = array()) {
  }

  public static function getAlias() {
    return 'testoafiles';
  }

  /**
   * Get the session from the parent context.
   */
  protected function getSession() {
    return $this->getMainContext()->getSession();
  }

  /**
   * @Given /^I press the file section cog$/
   */
  public function IPressTheFileSectionCog() {
    $driver = $this->getSession()->getDriver();
    $links = $driver->find("//div[contains(concat(' ', @class, ' '), ' oa-file-root ')]/a");
    if ($links) {
      $links[0]->click();
    }
    else {
      throw new \Exception(sprintf('Unable to find the file section cog on %s', $this->getSession()->getCurrentUrl()));
    }
  }

  /**
   * @Then /^I click to Add file$/
   */
  public function iClickToAddFile() {
    $driver = $this->getSession()->getDriver();
    $links = $driver->find("//div[contains(concat(' ', @class, ' '), ' oa-files-topbuttons ')]//a[contains(@href,'/oa-files/upload/ajax?menu_parent=') or contains(@href,'/oa-files/upload/ajax?term_parent=')]");
    if ($links) {
      $links[0]->click();
    }
    else {
      throw new \Exception(sprintf('Unable to find an add files link on the page %s', $this->getSession()->getCurrentUrl()));
    }
  }

  /**
   * @Given /^I click "([^"]*)" for folder "([^"]*)"$/
   */
  public function iClickForFolder($link_title, $folder_name) {
    $driver = $this->getSession()->getDriver();
    $links = $driver->find("//div[contains(concat(' ', @class, ' '), ' oa-files-folder ')]//div[contains(concat(' ', @class, ' '), 'tree-label ')]//span[contains(normalize-space(text()), '"  . $folder_name . "')]/../div[contains(concat(' ', @class, ' '), ' oa-file ')]/a");
    if ($links) {
      $links[0]->click();
      $links = $driver->find("//div[contains(concat(' ', @class, ' '), ' oa-files-folder ')]//div[contains(concat(' ', @class, ' '), 'tree-label ')]//span[contains(normalize-space(text()), '"  . $folder_name . "')]/../div[contains(concat(' ', @class, ' '), ' oa-file ')]/ul/li/a[contains(normalize-space(text()), '"  . $link_title . "')]");
      if ($links) {
        $links[0]->click();
      }
      else {
        throw new \Exception(sprintf('Unable to find %s link for folder %s on the page %s', $link_title, $folder_name, $this->getSession()->getCurrentUrl()));

      }
    }
    else {
      throw new \Exception(sprintf('Unable to find the folder %s on the page %s', $folder_name, $this->getSession()->getCurrentUrl()));
    }
  }

  /**
   * @Then /^I should see the file "([^"]*)" under "([^"]*)"$/
   * @Then /^I should see the folder "([^"]*)" under "([^"]*)"$/
   */
  public function iShouldSeeTheFileUnder($file, $folder_name) {
    $driver = $this->getSession()->getDriver();
    $links = $driver->find("//div[contains(concat(' ', @class, ' '), ' oa-files-folder ')]//div[contains(concat(' ', @class, ' '), 'tree-label ')]//span[contains(normalize-space(text()), '"  . $folder_name . "')]/../../treeitem/ul/li//span[contains(normalize-space(text()), '"  . $folder_name . "')]");
    if (!$file) {
      throw new \Exception(sprintf('The "%s" file was not found under %s on %s', $file, $folder_name, $this->getSession()->getCurrentUrl()));
    }
  }

  /**
   * @Then /^I should see the file "([^"]*)"$/
   */
  public function iShouldSeeTheFile($file_name) {
    $driver = $this->getSession()->getDriver();
    $row = $driver->find("//div[contains(concat(' ', @class, ' '), 'tree-label ')]//span[contains(normalize-space(text()), '"  . $file_name . "')]");
    if ($row && $row[0]->isVisible()) {
      return;
    }
    throw new \Exception(sprintf('The "%s" file was not found or not visible on %s', $file_name, $this->getSession()->getCurrentUrl()));
  }

  /**
   * @Then /^I should not see the file "([^"]*)"$/
   */
  public function iShouldNotSeeTheFile($file_name) {
    $driver = $this->getSession()->getDriver();
    $row = $driver->find("//div[contains(concat(' ', @class, ' '), 'tree-label ')]//span[contains(normalize-space(text()), '"  . $file_name . "')]");
    if (!$row || !$row[0]->isVisible()) {
      return;
    }
    throw new \Exception(sprintf('The "%s" file is visible on %s', $file_name, $this->getSession()->getCurrentUrl()));
  }
}
