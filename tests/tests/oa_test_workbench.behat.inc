<?php
/**
 * @file
 * Provide Behat step-definitions for files/media in OA.
 *
 */

use Drupal\DrupalExtension\Context\DrupalSubContextInterface;
use Behat\Behat\Context\BehatContext;
use Drupal\Component\Utility\Random;
use Behat\Behat\Event\StepEvent;
use Behat\Mink\Exception\ElementNotFoundException;

class TestOaWorkbenchSubContext extends BehatContext implements DrupalSubContextInterface {

  // Keep track of the previous node_options state.
  protected $node_options = array();

  /**
   * Initializes context.
   */
  public function __construct(array $parameters = array()) {
  }

  public static function getAlias() {
    return 'testoaworkbench';
  }

  /**
   * Get the session from the parent context.
   */
  protected function getSession() {
    return $this->getMainContext()->getSession();
  }

  /**
   * @Given /^I have moderation enabled for "([^"]*)"$/
   */
  public function iHaveModerationEnabledFor($arg1) {
    if (!module_exists('oa_workbench')) {
      module_enable(array('oa_workbench'));
    }
    $options = variable_get('node_options_' . $arg1, array());
    if (!in_array('moderation', $options)) {
      if (!isset($this->node_options[$arg1])) {
        $this->node_options[$arg1] = $options;
      }
      $options[] = 'moderation';
      variable_set('node_options_' . $arg1, $options);
    }
  }

  /**
   * @AfterScenario
   *
   * Undo workbench moderation.
   */
  public function removeModeration() {
    if (module_exists('oa_workbench')) {
      foreach ($this->node_options as $node_type => $options) {
        variable_set('node_options_' . $node_type, $options);
      }
      // Delete any added states
      drupal_static_reset('workbench_moderation_states');
      foreach (workbench_moderation_states() as $machine_name => $state) {
        if (strpos($machine_name, 'state_') === 0 && is_numeric(substr($machine_name, 6))) {
          workbench_moderation_state_delete($state);
        }
      }
      entity_get_controller('workbench_moderation_profile')->resetCache();
      foreach (workbench_moderation_profile_get_profiles() as $machine_name => $profile) {
        if (strpos($machine_name, 'profile_') === 0 && is_numeric(substr($machine_name, 8))) {
          workbench_moderation_profile_delete($profile);
        }
      }
    }
  }
}
