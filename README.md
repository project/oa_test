Behat tests
===========

Quick setup
-----------

(Be in tests subdirectory)

 1. Install Composer

    php -r "eval('?>'.file_get_contents('https://getcomposer.org/installer'));"
 
 2. Install Behat and dependencies via Composer

    php composer.phar install

 3. Copy behat.template.yml to behat.yml and modify

    mv behat.template.yml behat.yml

 4. You will need to use panopoly_test-7.x-1.20 to run these tests

    Remove the panopoly_test directory and git clone --branch 7.x-1.x http://git.drupal.org/project/panopoly_test.git

    git checkout 7.x-1.20
 
 5. Enable the oa_test module (this will also enable panopoly_test)

    drush en oa_test

 6. Run Behat and examine test results!
 
    bin/behat

Profiles
-----------

Some of the tests need to run on Chrome due to issues in the Selenium Firefox 
driver. To run the tests tagged with @chrome, you need to run Behat using the 
Chrome profile:

    bin/behat --profile chrome

More information
----------------

For detailed instructions, see:

  https://drupal.org/node/2271009


Development debugging:
--
Add to behat
    Behat\MinkExtension\Extension:
      show_auto: true
      show_cmd: firefox %s

For OSX use open -a Firefox %s instead of firefox %s.

Use "Then show last response" to open a screenshot.
